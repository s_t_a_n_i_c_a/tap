export default state => {
    if(state.user.user && state.user.user.profiles && state.user.user.details) {
        return state.user.user
    }
    else {
        return null;
    }
}
