import { actionTypes } from 'actions/UserActions';

const initialState = {
  user: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_REQUEST:
      return {
        ...state,
      };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        user: action.user,
      };
    case actionTypes.REGISTER_REQUEST:
      return {
        ...state,
      };
    case actionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        user: action.user,
      };
    case actionTypes.LOGOUT:
      return initialState;
    
    case actionTypes.AUTHCHECK_REQUEST:
      return {
        ...state,
      };
    case actionTypes.AUTHCHECK_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          pubkey: action.auth.pubkey
        }
      };
    case actionTypes.AUTHCHECK_ERROR:
      return {
        ...state,
        user: {
          ...state.user,
          error: action.error
        }
      }
    case actionTypes.PROFILE_REQUEST:
      return {
        ...state,
      };
    case actionTypes.PROFILE_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          profiles: action.profiles
        }
      };
    case actionTypes.DETAILS_REQUEST:
      return {
        ...state,
      };
    case actionTypes.DETAILS_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          details: action.details
        }
      };
    case actionTypes.TIMESYNC_REQUEST:
      return {
        ...state
      };
    case actionTypes.TIMESYNC_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          timeSyncId: action.id
        }
      };
    // case actionTypes.TIMESYNC_ERROR:
    //     return {
    //       ...state,
    //       user: {
    //         ...state.user,
    //         timeSyncId: null
    //       }
    //     }
    default:
      return state;
  }
};

export default userReducer;
