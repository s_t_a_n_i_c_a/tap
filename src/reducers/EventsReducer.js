import { actionTypes } from 'actions/EventsActions';

const initialState = {
  events: []
};

const eventsReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SEARCH_REQUEST:
            return {
                ...state,
            };
        case actionTypes.SEARCH_SUCCESS:
            return {
                ...state,
                events: action.results
            }
        case actionTypes.SEARCH_ERROR:
            return {
                ...state,
                events: {
                    ...state.events,
                    error: action.error
                }
            }
        default:
        return state;
    }
};

export default eventsReducer;