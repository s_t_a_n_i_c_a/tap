import { combineReducers } from 'redux';
import error from './ErrorReducer';
import user from './UserReducer';
import status from './StatusReducer';
import events from './EventsReducer';

const rootReducer = combineReducers({
  error,
  user,
  status,
  events
});

export default rootReducer;
