import { eventsClient } from './HttpClient';
import { cjson } from '../services';

const events = {
    search: '/?c=21'
};

class EventsController {
  constructor() {
    
  }

  search = async (device, query) => {
    if(query){
        const result = await eventsClient.post('https://us.crowdblink.com/api_patron_v1' + events.search, query, {
            headers: {
                'Authorization': 'CRB ' + device,
                'Content-Type': 'text/plain',
            }
        });
        if(result.data[0] === 'C')
            return cjson.parse(result.data);
        else return result.data;
    }
    else {
        try {
            const result = await eventsClient.get('https://us.crowdblink.com/api_patron_v1' + events.search, {headers: {'Authorization': 'CRB ' + device}});
            if(result.data[0] === 'C')
                return cjson.parse(result.data);
            else return result.data;
        }
        catch (error) {
            throw(error);
        }
    }
  };
}

export default new EventsController();