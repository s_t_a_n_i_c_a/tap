import { authClient } from './HttpClient';
import { cjson } from '../services';

const user = {
  login: '/?c=1',
  authCheck: '/?c=3',
  profile: '/?c=81',
  details: '/?c=13',
  create: '/?c=11'
}

class UserController {
  constructor() {
    
  }

  login = async (email, password) =>
  // Real implementation of a login request using the HttpClient
    {
      try {
        var bodyFormData = new FormData();
        bodyFormData.append('login', email);
        bodyFormData.append('password', password);
        bodyFormData.append('deviceName', 'personal cell');
        const result = await authClient.post(
          'https://ca.crowdblink.com/api_patron_v1' + user.login,
          bodyFormData,
        );
        return result.data;
        // Data is the object exposes by axios for the response json
      } catch (error) {
        throw(error);
      }
    }
    
    // This is a mocked example to simulate api behavior
    // new Promise((resolve, reject) => {
    //   if (email !== 'a@a.com' && password !== '') {
    //     setTimeout(
    //       () => resolve({ name: 'Milan' }),
    //       1000,
    //     );
    //   } else {
    //     setTimeout(
    //       () => reject(new Error('Invalid Email/Password')),
    //       1000,
    //     );
    //   }
    // });

  logout = () => null;

  authCheck = async (device) => {
    try {
      const result = await authClient.get('https://ca.crowdblink.com/api_patron_v1' + user.authCheck, {headers: {'Authorization': 'CRB ' + device}});
      return result.data;
    }
    catch (error) {
      throw(error);
    }
  };

  profile = async (device) => {
    try {
      const result = await authClient.get('https://ca.crowdblink.com/api_patron_v1' + user.profile, {headers: {'Authorization': 'CRB ' + device}});
      let profiles = [];
      if(!result.data.length) {
        profiles = cjson.parse(result.data);
      }
      else {
        profiles = result.data;
      }
      let profileDetails = [];
      for(let i=0; i<profiles.length; i++){
        let profileDetail = await authClient.get('https://ca.crowdblink.com/api_patron_v1' + user.profile + '&&i=' + profiles[i].id, {headers: {'Authorization': 'CRB ' + device}});
        profileDetails.push(profileDetail.data);
      }
      return profileDetails;
    }
    catch (error) {console.log(error)
      throw(error);
    }
  }

  profileDetails = async (device, id) => {
    try {
      const result = await authClient.get('https://ca.crowdblink.com/api_patron_v1' + user.profile + '&&i=' + id, {headers: {'Authorization': 'CRB ' + device}});
      return result.data;
    }
    catch (error) {
      throw(error);
    }
  }

  details = async (device) => {
    try {
      const result = await authClient.get('https://ca.crowdblink.com/api_patron_v1' + user.details, {headers: {'Authorization': 'CRB ' + device}});
      return result.data;
    }
    catch (error) {
      throw(error);
    }
  }

  timeSync = async (device) => {
    try {
      const result = await authClient.get('https://ca.crowdblink.com/api_patron_v1' + user.authCheck + '&x=time', {
        headers: {
            'Authorization': 'CRB ' + device,
            "CLIENT-TIME": parseInt(Date.now()/1000)
          }
        });
        let diff = parseInt(result.data);
        console.log('sync')
        diff = parseInt(Date.now()/1000) - diff;
        if (diff > 5){
          this.timeSync(device);
        }
    }
    catch (error) {
      throw(error);
    }
  };

  register = async (email, password, name, device) => {
    try {
      var bodyFormData = new FormData();
      bodyFormData.append('deviceName', device);
      bodyFormData.append('name', name);
      bodyFormData.append('email', email);
      bodyFormData.append('yob', 1990);
      bodyFormData.append('gender', 1);
      bodyFormData.append('phone', '4165550000');

      const result = await authClient.post(
        user.create,
        bodyFormData,
      );
      console.log(result);
      return result.data;
    }
    catch (error) {
      throw(error);
    }
  }
}

export default new UserController();
