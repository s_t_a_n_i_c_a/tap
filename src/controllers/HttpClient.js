import axios from 'axios';
import { IDENTITY_URL, AUTH } from 'react-native-dotenv';
/*
  Base client config for your application.
  Here you can define your base url, headers,
  timeouts and middleware used for each request.
*/

const requestHandler = (error) => {
  console.log('Failed to make request with error:');
  console.log(error.response);
  return Promise.reject(error);
}

const responseHandler = (error, e) => {
  console.log('Request got response with error:');
  console.log(error.response);
  return Promise.reject(error);
}

export const idClient = axios.create({
  baseURL: IDENTITY_URL,
  timeout: 100000,
  headers: { 'content-type': 'application/json' },
});

// Custom middleware for requests (this one just logs the error).
idClient.interceptors.request.use(config => config, requestHandler);

// Custom middleware for responses (this one just logs the error).
idClient.interceptors.response.use(response => response, responseHandler);

export const authClient = axios.create({
  baseURL: AUTH,
  timeout: 100000,
  headers: {'Content-Type': 'multipart/form-data' },
});

// Custom middleware for requests (this one just logs the error).
authClient.interceptors.request.use(config => config, requestHandler);

// Custom middleware for responses (this one just logs the error).
authClient.interceptors.response.use(response => response, responseHandler);

export const eventsClient = axios.create({
  baseURL: AUTH,
  timeout: 100000,
  headers: {'Content-Type': 'application/json' },
});

// Custom middleware for requests (this one just logs the error).
eventsClient.interceptors.request.use(config => config, requestHandler);

// Custom middleware for responses (this one just logs the error).
eventsClient.interceptors.response.use(response => response, responseHandler);