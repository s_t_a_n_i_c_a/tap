import UserController from '../controllers/UserController';

export const actionTypes = {
  LOGIN: 'LOGIN',
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_ERROR: 'LOGIN_ERROR',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  REGISTER: 'REGISTER',
  REGISTER_REQUEST: 'REGISTER_REQUEST',
  REGISTER_ERROR: 'REGISTER_ERROR',
  REGISTER_SUCCESS: 'REGISTER_SUCCESS',
  REGISTER_ERROR: 'REGISTER_ERROR',
  LOGOUT: 'LOGOUT',
  AUTHCHECK: 'AUTHCHECK',
  AUTHCHECK_REQUEST: 'AUTHCHECK_REQUEST',
  AUTHCHECK_SUCCESS: 'AUTHCHECK_SUCCESS',
  AUTHCHECK_ERROR: 'AUTHCHECK_ERROR',
  PROFILE: 'PROFILE',
  PROFILE_REQUEST: 'PROFILE_REQUEST',
  PROFILE_SUCCESS: 'PROFILE_SUCCESS',
  PROFILE_ERROR: 'PROFILE_ERROR',
  DETAILS: 'DETAILS',
  DETAILS_REQUEST: 'DETAILS_REQUEST',
  DETAILS_SUCCESS: 'DETAILS_SUCCESS',
  DETAILS_ERROR: 'DETAILS_ERROR',
  TIMESYNC: 'TIMEYNC',
  TIMESYNC_REQUEST: 'TIMESYNC_REQUEST',
  TIMESYNC_SUCCESS: 'TIMESYNC_SUCCESS',
  TIMESYNC_ERROR: 'TIMESYNC_ERROR',
};

const loginRequest = () => ({
  type: actionTypes.LOGIN_REQUEST,
});

const loginError = error => ({
  type: actionTypes.LOGIN_ERROR,
  error,
});

const loginSuccess = user => ({
  type: actionTypes.LOGIN_SUCCESS,
  user,
});

const logoutRequest = () => ({
  type: actionTypes.LOGOUT,
});

const registerRequest = () => ({
  type: actionTypes.REGISTER_REQUEST
});

const registerError = error => ({
  type: actionTypes.REGISTER_ERROR,
  error,
});

const registerSuccess = user => ({
  type: actionTypes.REGISTER_SUCCESS,
  user,
});

const authCheckRequest = () => ({
  type: actionTypes.AUTHCHECK_REQUEST,
});

const authCheckSuccess = auth => ({
  type: actionTypes.AUTHCHECK_SUCCESS,
  auth,
})

const authCheckError = error => ({
  type: actionTypes.AUTHCHECK_ERROR,
  error,
})

const profileRequest = () => ({
  type: actionTypes.PROFILE_REQUEST,
});

const profileSuccess = profiles => ({
  type: actionTypes.PROFILE_SUCCESS,
  profiles,
})

const profileError = error => ({
  type: actionTypes.PROFILE_ERROR,
  error,
})

const detailsRequest = () => ({
  type: actionTypes.PROFILE_REQUEST,
});

const detailsSuccess = details => ({
  type: actionTypes.DETAILS_SUCCESS,
  details,
})

const detailsError = error => ({
  type: actionTypes.DETAILS_ERROR,
  error,
})

const timeSyncRequest = () => ({
  type: actionTypes.TIMESYNC_REQUEST
})

const timeSyncSuccess = (id) => ({
  type: actionTypes.TIMESYNC_SUCCESS,
  id
})

const timeSyncError = () => ({
  type: actionTypes.TIMESYNC_ERROR
})

export const login = (email, password) => async (dispatch) => {
  dispatch(loginRequest());
  let user = {};
  try {
    user = await UserController.login(email, password);
    dispatch(loginSuccess(user));
  } catch (error) {
    dispatch(loginError(error.response.data));
  }

  timeSync(user.device)(dispatch);
  profiles(user.device)(dispatch);
  details(user.device)(dispatch);
  
  
};

export const hack = (device) => async (dispatch) => {
  console.log('<<<<<<<<<<<<<<<<< fuck cb')
  timeSync(device)(dispatch);
  profiles(device)(dispatch);
  details(device)(dispatch);
};

export const register = (email, password, name, device) => async (dispatch) => {
  dispatch(registerRequest());
  let user = {};
  try {
    user = await UserController.register(email, password, name, device);
    console.log(user);
    dispatch(registerSuccess(user));
  }
  catch (error) {
    dispatch(registerError("Something went horribly, horribly wrong"));
  }
};

export const logout = () => (dispatch) => {
  UserController.logout();
  dispatch(logoutRequest());
};

export const authCheck = (device) => async (dispatch) => {
  dispatch(authCheckRequest())
  try {
    const auth = await UserController.authCheck(device);
    dispatch(authCheckSuccess(auth));
  }
  catch (error) {
    dispatch(authCheckError("Something went wrong. Please try again later."));
  }
};

export const profiles = (device) => async (dispatch) => {
  dispatch(profileRequest());
  try {
    const profile = await UserController.profile(device);
    dispatch(profileSuccess(profile));
  }
  catch (error){
    dispatch(profileError("Something went wrong. Please try again later."));
  }
};

export const details = (device) => async (dispatch) => {
  dispatch(detailsRequest());
  try {
    const details = await UserController.details(device);
    dispatch(detailsSuccess(details));
  }
  catch (error) {
    dispatch(detailsError("Something went wrong. Please try again later."));
  }
};

export const timeSync = (device) => async (dispatch) => {
  dispatch(timeSyncRequest());
  try {
    await UserController.timeSync(device);
    let syncId = setInterval(() => UserController.timeSync(device),1800*1000);
    dispatch(timeSyncSuccess(syncId));
  }
  catch (error) {
    dispatch(timeSyncError(error.response.data));
  }
};