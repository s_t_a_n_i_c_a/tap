import EventsController from '../controllers/EventsController';

export const actionTypes = {
    SEARCH: 'SEARCH',
    SEARCH_REQUEST: 'SEARCH_REQUEST',
    SEARCH_SUCCESS: 'SEARCH_SUCCESS',
    SEARCH_ERROR: 'SEARCH_ERROR',
};

const searchRequest = () => ({
    type: actionTypes.SEARCH_REQUEST
});

const searchError = error => ({
    type: actionTypes.SEARCH_ERROR,
    error
});

const searchSuccess = results => ({
    type: actionTypes.SEARCH_SUCCESS,
    results
})

export const search = (device, query) => async (dispatch) => {
    dispatch(searchRequest());
    let results = [];
    try {
        results = await EventsController.search(device, query);
        console.log(results)
        dispatch(searchSuccess(results));
    } catch (error) {
        dispatch(searchError(error.response.data));
    }
};