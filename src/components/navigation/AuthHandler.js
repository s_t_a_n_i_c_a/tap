import React, { useEffect, useCallback } from 'react';
import {
  View,
  StyleSheet,
  Image
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import logo from 'assets/images/logo_black.png';
import PropTypes from 'prop-types';
import getUser from 'selectors/UserSelectors';
import Colors from 'helpers/Colors';
import { authCheck, timeSync, details, profiles } from 'actions/UserActions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  logo: {
    width: '100%',
    height: 90,
    resizeMode: 'contain',
  },
});

function AuthHandler(props) {
  const user = useSelector(state => getUser(state));
  if(user && user.timeSyncId){
    clearInterval(user.timeSyncId);
  }
  const dispatch = useDispatch();
  const _timeSync = useCallback((device) => dispatch(timeSync(device)), [])
  const _authCheck = useCallback((device) => dispatch(authCheck(device)), []);
  const _profiles = useCallback((device) => dispatch(profiles(device)), []);
  const _details = useCallback((device) => dispatch(details(device)), []);

  useEffect(() => {
    if (user !== null) {
      (async () => {
        await authCheck(user.device);
        await timeSync(user.device);
        await profiles(user.device);
        await details(user.device);
        props.navigation.navigate('Home');
      })();
    } else {
      props.navigation.navigate('Login');
    }
  });

  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.logo}/>
    </View>
  );
}

AuthHandler.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default AuthHandler;
