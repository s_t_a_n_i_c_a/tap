import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import AuthHandler from './AuthHandler';
import Auth from './AuthNavigator';
import App from './AppNavigator';
import Onboarding from '../Onboarding';
import Registration from '../Registration';
import Register from '../Register';

export default createAppContainer( // eslint-disable-line
  createSwitchNavigator(
    {
      App,
      Auth,
      AuthHandler,
      Onboarding,
      Registration,
      Register
    },
    {
      initialRouteName: 'AuthHandler',
    },
  ),
); // eslint-disable-line
