import React from 'react';
import { Image, Text } from 'react-native';
import { Icon } from 'native-base';
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import Home from '../Home';
import Profile from '../Profile';
import Wallet from '../Wallet';
import TicketInfo from '../Wallet/TicketInfo';
import Music from '../Wallet';

import Colors from 'helpers/Colors';

const iconForTab = ({ state }) => {
  switch (state.routeName) {
    case 'Home':
      return 'home';
    case 'Profile':
      return 'user';
    case 'Music':
      return 'music';
    case 'Wallet':
      return 'music';
    default:
      return null;
  }
};

const TabIcon = ({ icon, tintColor }) => (// eslint-disable-line
  <Image
    source={icon}
    style={{ tintColor }}
  />
);

const ProfileStack = createStackNavigator({ Profile }, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: Colors.primary,
    },
    headerTintColor: Colors.white,
  }
});

const HomeStack = createStackNavigator({ Home : {
  screen: Home,
  navigationOptions: ({navigation}) => {
    return {
      header: null
    }
  }
}}, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: Colors.primary,
    },
    headerTintColor: Colors.white,
  }
});

const WalletStack = createStackNavigator({ Wallet, TicketInfoScreen: {
  screen: TicketInfo,
  navigationOptions: ({navigation}) => {
    return {
      headerTransparent: true,
    }
  }
} }, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: Colors.primary,
    },
    headerBackTitle: 'Tickets',
    headerTintColor: Colors.white,
  }
});
const MusicStack = createStackNavigator({ Music }, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: Colors.primary,
    },
    headerTintColor: Colors.white,
  }
});
const AppStack = createBottomTabNavigator(
  {
    Home: HomeStack,
    Profile: ProfileStack,
    Wallet: WalletStack
  },
  {
    tabBarPosition: 'bottom',
    tabBarOptions: {
      activeTintColor: Colors.primary,
      inactiveTintColor: Colors.gray,
      showLabel: false,
      animationEnabled: true,
      style: {
        shadowColor: 'rgba(58,55,55,0.1)',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 15,
        elevation: 3,
        borderTopColor: 'transparent',
        height: 50
      },
      labelStyle: {
        justifyContent: 'flex-start',
      },
    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => (// eslint-disable-line
        <Icon
          name={iconForTab(navigation)}
          style={{color: tintColor}}
          type='Entypo'
        />
      ),
    }),
  },
);

export default AppStack;
