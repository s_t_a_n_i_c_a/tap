import React, { useCallback, useEffect, useState } from 'react';
import {
  View,
  StatusBar,
} from 'react-native';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import SwiperFlatList from 'react-native-swiper-flatlist';

import styles from './styles';
import Pagination from 'helpers/Pagination';
import Page1 from './page1';
import Page2 from './page2';
import Page3 from './page3';
import Page4 from './page4';

import getUser from 'selectors/UserSelectors';
import strings from 'localization';
import { getStatusBarHeight } from 'react-native-status-bar-height';

function Onboarding(props) {

  const user = useSelector(state => getUser(state));

  const login = (value) => props.navigation.navigate('Login')

  useEffect(() => {
    if (user !== null) {
      props.navigation.navigate('App');
    }
  });

  return (
    <View style={styles.container}>
      <StatusBar  hidden/>
      <View style={[styles.formContainer]}>
        {/* <Image source={logo} style={styles.logo}/> */}
        <View style={{flex:1, marginBottom: 20}}>
            <SwiperFlatList
            showPagination
            PaginationComponent={Pagination}
            >
              <View style={[styles.child]}>
                <Page1/>
              </View>
              <View style={[styles.child]}>
              < Page2/>
              </View>
              <View style={[styles.child]}>
              < Page3/>
              </View>
              <View style={[styles.child]}>
              < Page4/>
              </View>
          </SwiperFlatList>
        </View>
      </View>
      {/* <View style={styles.loginContainer}>
        {/* <Button
          block
          onPress={login}
          style={styles.loginButton}
        >
          <Text style={styles.loginButtonText}> {strings.login}</Text>
        </Button> 
        <Button
          bordered
          block
          onPress={login}
          style={[styles.loginButton]}
        >
          <Text style={styles.loginButtonText}> {strings.register}</Text>
        </Button>
      </View> */}
    </View>
  );
}

Onboarding.navigationOptions = {
  headerStyle: {
    margin: Platform.OS === 'ios' ? -getStatusBarHeight() :  -getStatusBarHeight() - 4,
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
    elevation: 0
  },
  // headerTitle: <Thumbnail style={styles.headerImage} source={{uri: 'https://iukl.edu.my/wp-content/uploads/2018/01/bachelor-of-communication-hons-in-corporate-communication-1.jpg'}} square/>
};

Onboarding.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Onboarding;