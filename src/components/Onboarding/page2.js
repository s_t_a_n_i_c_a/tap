import React, { Component, useState, useEffect } from "react";
import Colors from 'helpers/Colors';
import { StyleSheet, View, Text, Image } from "react-native";
import { Icon, Button } from 'native-base';
import logo from 'assets/images/icon.png';
import strings from 'localization';
import posed from 'react-native-pose';

export default function Page2(props){
  return (
    <View style={styles.root}>
      <View style={styles.top}>
        <Icon style={styles.logo} type='EvilIcons' name='lock'/>
        <Text style={styles.headerText}>{strings.onboarding.page2.header}</Text>
      </View>
      <View style={styles.bottomContainer}>
        <View style={styles.bottomContainerText}>
          <Text style={styles.bodyText}>{strings.onboarding.page2.instructions}</Text>
          <View style = {styles.lineStyle} />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "white",
  },
  top: {
    backgroundColor: Colors.primary,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    fontSize: 150,
    color: 'white',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 25,
    color: Colors.white
  },
  bodyText: {
    fontSize: 20,
    color: Colors.black,
    textAlign: 'center',
    lineHeight: 27
  },
  bottomContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomContainerText: {
    marginBottom: '5%',
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    paddingHorizontal: 50,
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor: Colors.black,
    marginTop:30,
    width: 40,
  }
});
