import React, { Component } from "react";
import Colors from 'helpers/Colors';
import { StyleSheet, View, Text, Image } from "react-native";
import PropTypes from 'prop-types';
import { Icon, Button } from 'native-base';
import logo from 'assets/images/icon.png';
import strings from 'localization';
import { withNavigation } from 'react-navigation';

function Page1 (props) {
  const register = (value) => props.navigation.navigate('Registration')
  const login = () => props.navigation.navigate('Login');

  return (
    <View style={styles.root}>
      <View style={styles.topContainer}>
        <Image source={logo} style={styles.logo}/>
        <Text style={styles.headerText}>{strings.onboarding.page1.header}</Text>
      </View>
      <View style={styles.bottomContainer}>
        <View style={{flex: 1, justifyContent: 'flex-end', alignSelf:'stretch'}}>
          <Button block style={styles.register} onPress={register}>
            <Text style={styles.registerText}> {strings.register}</Text>
          </Button>
          <View style={{flexDirection: 'row', margin: 30}}>
              <View style={{backgroundColor: Colors.gray, height: 1, flex: 1, alignSelf: 'center'}} />
              <Text style={{ color: Colors.gray, alignSelf:'center', paddingHorizontal:5, fontSize: 15 }}>Or</Text>
              <View style={{backgroundColor: Colors.gray, height: 1, flex: 1, alignSelf: 'center'}} />
          </View>
          <Button block style={styles.login} onPress={login}>
            <Text style={styles.loginText}> {strings.login}</Text>
          </Button>
        </View>
        <View style={styles.bottomContainerText}>
          <Text style={styles.bodyText}>{strings.onboarding.page1.instructions}</Text>
          <Text style={styles.bodyText}>{strings.onboarding.page1.instructionsSubtext}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "white",
  },
  topContainer: {
    backgroundColor: Colors.primary,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    height: 50,
    resizeMode: 'contain',
    marginTop: '10%'
  },
  headerText: {
    marginTop: '10%',
    fontWeight: 'bold',
    fontSize: 24,
    color: Colors.white,
    fontFamily:'Lato-Regular'
  },
  bodyText: {
    fontSize: 15,
    color: Colors.gray
  },
  bottomContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomContainerText: {
    marginBottom: '3%',
    flex: 1,
    alignItems:'center',
    justifyContent:'flex-end'
  },
  register: {
    borderColor: Colors.hero,
    backgroundColor: Colors.hero,
    borderRadius: 0,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    marginHorizontal: 30
  },
  login: {
    borderColor: Colors.hero,
    backgroundColor: Colors.white,
    borderRadius: 0,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    marginHorizontal: 30,
  },
  registerText: {
    fontWeight: 'bold',
    color: Colors.white,
    letterSpacing: 3
  },
  loginText: {
    fontWeight: 'bold',
    color: Colors.hero,
    letterSpacing: 3
  },
});

Page1.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default withNavigation(Page1);