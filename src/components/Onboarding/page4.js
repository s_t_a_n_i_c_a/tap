import React, { Component } from "react";
import Colors from 'helpers/Colors';
import { StyleSheet, View, Text, Image } from "react-native";
import { Icon, Button } from 'native-base';
import logo from 'assets/images/icon.png';
import strings from 'localization';
import { withNavigation } from 'react-navigation';

function Page4(props) {
  const register = (value) => props.navigation.navigate('Registration')

  return (
    <View style={styles.root}>
      <View style={styles.top}>
        <Image source={logo} style={styles.logo}/>
      </View>
      <View style={styles.bottomContainer}>
        <Text style={styles.bodyText}>{strings.onboarding.page4.instructions}</Text>
        <View>
          <Button block style={styles.register} onPress={register}>
            <Text style={styles.registerText}> {strings.create}</Text>
          </Button>
          <Text style={styles.bottomContainerText}>{strings.onboarding.page4.instructionsSubtext}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "white",
  },
  top: {
    backgroundColor: Colors.primary,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    height: 100,
    resizeMode: 'contain',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 25,
    color: Colors.white,
    paddingHorizontal: 30,
    textAlign: 'center'
  },
  bodyText: {
    fontSize: 25,
    color: Colors.black,
    textAlign: 'center',
    lineHeight: 27
  },
  bottomContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: 50,
  },
  bottomContainerText: {
    fontSize: 12,
    color: Colors.gray,
    marginTop: '5%',
    alignItems:'center',
    justifyContent:'center',
    textAlign: 'center',
    paddingHorizontal: 10,
  },
  register: {
    borderColor: Colors.hero,
    backgroundColor: Colors.hero,
    borderRadius: 0,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
  },
  registerText: {
    fontWeight: 'bold',
    color: Colors.white,
    letterSpacing: 3
  }
});

export default withNavigation(Page4);