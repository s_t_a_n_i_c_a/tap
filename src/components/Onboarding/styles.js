import { StyleSheet } from 'react-native';
import Colors from 'helpers/Colors';
import { Dimensions } from 'react-native';

export const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,1)'
  },
  formContainer: {
    justifyContent: 'space-between',
    // marginHorizontal: 10,
    // marginTop: 100,
    flex: 1
  },
  loginContainer: {
    marginTop: 40,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logo: {
    width: '100%',
    height: 80,
    resizeMode:
    'contain',
    marginTop: 40
  },
  loginButton: {
    borderRadius: 0,
    borderWidth: 0,
    height: 70,
    backgroundColor: Colors.primary,
    borderColor: 'transparent',
    elevation: 0,
    flex: 1
    // marginHorizontal: 10,
  },
  loginButtonText: {
    color: Colors.black,
    letterSpacing: 1,
    fontSize: 18
  },
  child: {
    width,
    justifyContent: 'center'
  },
  text: {
    fontSize: width * 0.5,
    textAlign: 'center'
  }
});

export default styles;
