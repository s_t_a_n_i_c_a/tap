import React from 'react';
import renderer from 'react-test-renderer';
import Button from '../Button';

describe('Common button', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Button title="Test Button"/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});