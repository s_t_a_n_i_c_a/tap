import React, { useCallback, useEffect, useState } from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  ImageBackground
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Colors from 'helpers/Colors';

import ErrorView from '../common/ErrorView';
import styles from './styles';
import logo from 'assets/images/logo_black.png';

import getUser from 'selectors/UserSelectors';
import errorsSelector from 'selectors/ErrorSelectors';
import { isLoadingSelector } from 'selectors/StatusSelectors';
import strings from 'localization';
import { register, actionTypes } from 'actions/UserActions';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Button, Icon, Item, Input} from 'native-base';

function Register(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [device, setDevice] = useState('');
  const [name, setName] = useState('');

  const user = useSelector(state => getUser(state));
  const isLoading = useSelector(state => isLoadingSelector([actionTypes.REGISTER], state));
  const errors = useSelector(state => errorsSelector([actionTypes.REGISTER, actionTypes.PROFILE], state));

  const dispatch = useDispatch();
  const registerUser = useCallback(() => dispatch(register(email, password, name, device)), [email, password, name, device, dispatch]);
  const passwordChanged = useCallback(value => setPassword(value), []);
  const emailChanged = useCallback(value => setEmail(value), []);
  const deviceChanged = useCallback(value => setDevice(value), []);
  const nameChanged = useCallback(value => setName(value), []);

  useEffect(() => {
    if (user !== null) {
      props.navigation.navigate('Home');
    }
  });

  return (
    <View style={styles.container}>
      <StatusBar  hidden/>
      <View style={styles.formContainer}>
        <View style={styles.header}>
          <Image source={logo} style={styles.logo}/>
        </View>
        <View style={{flex:2}}>
          <Item style={styles.formField}>
            <Icon active type="Entypo" name='user' style={styles.loginIcon}/>
            <Input
              style={styles.textField}
              placeholder="Name"
              placeholderTextColor={Colors.muted}
              onChangeText={nameChanged}
              value={name}/>
          </Item>
          <Item style={styles.formField}>
            <Icon active type="Entypo" name='mobile' style={styles.loginIcon}/>
            <Input
              style={styles.textField}
              placeholder={strings.device}
              placeholderTextColor={Colors.muted}
              onChangeText={deviceChanged}
              value={device}/>
          </Item>
          <Item style={styles.formField}>
            <Icon active name='mail' style={styles.loginIcon}/>
            <Input
              style={styles.textField}
              placeholder={strings.email}
              placeholderTextColor={Colors.muted}
              onChangeText={emailChanged}
              value={email}/>
          </Item>
          <Item style={styles.formField}>
            <Icon active name='lock' style={styles.loginIcon}/>
            <Input
              style={styles.textField}
              placeholder={strings.password}
              placeholderTextColor = {Colors.muted}
              value={password}
              onChangeText={passwordChanged}
              secureTextEntry/>
          </Item>
          <ErrorView errors={errors} />
          <View style={styles.extraButtonsContainer}>
          </View>
        </View>
      </View>
      <View style={styles.loginContainer}>
        <Button
          block
          onPress={registerUser}
          style={styles.loginButton}
        >
          <Text style={styles.loginButtonText}> {isLoading ? strings.loading : strings.register}</Text>
        </Button>
      </View>
    </View>
  );
}

Register.navigationOptions = {
  headerStyle: {
    margin: Platform.OS === 'ios' ? -getStatusBarHeight() :  -getStatusBarHeight() - 4,
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
    elevation: 0
  },
  // headerTitle: <Thumbnail style={styles.headerImage} source={{uri: 'https://iukl.edu.my/wp-content/uploads/2018/01/bachelor-of-communication-hons-in-corporate-communication-1.jpg'}} square/>
};

Register.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Register;
