import React from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native';
import { Icon, Button } from 'native-base';
import Colors from 'helpers/Colors';

function Ticket(props) {
    const authCheck = () => {
        props.onPress(props.options.id);
      }
    return (
        <TouchableOpacity style={styles.root} onPress={() => authCheck()}>
            <ImageBackground source={{uri: props.options.image}} style={styles.imageBackground}>
                <View style={styles.overlay}></View>
                <View style={styles.dateContainer}>
                    <View style={styles.dateBox}>
                        <Text style={styles.day}>{props.options.day}</Text>
                        <Text style={styles.month}>{props.options.month.substring(0,3).toUpperCase()}</Text>
                    </View>
                </View>
                <View style={styles.lowerContainer}>
                    <Text style={styles.location}>{props.options.location.toUpperCase()}</Text>
                    <Text style={styles.title}>{props.options.title}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icon style={styles.icon} type='Entypo' name='note'/>
                        <Text style={styles.place}>{props.options.place}</Text>
                    </View>
                </View>
            </ImageBackground>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    root: {
        height: 220,
        shadowColor: 'rgba(58,55,55,1)',
        shadowOffset: { width: 0, height: 7 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        padding: 10,
    },
    imageBackground: {
        flex: 1,
        height: 220,
        overflow: 'hidden',
        borderRadius: 20,
        justifyContent: 'space-between',
    },
    overlay: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.5)',
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        borderRadius: 20,
    },
    dateBox: {
        backgroundColor: Colors.white,
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 8,
        alignItems: 'center',
        borderRadius: 20,
    },
    dateContainer: {
        justifyContent: 'flex-end',
        flexDirection:'row',
        padding: 10
    },
    lowerContainer: {
        justifyContent: 'flex-end',
        padding: 10
    },
    day: {
        fontSize: 18,
        color: Colors.black,
    },
    month: {
        fontSize: 10,
        color: Colors.black,
        fontWeight: 'bold'
    },
    image: {
        position: 'absolute',
        left: 10,
        top: 0,
    },
    location: {
        fontSize: 10,
        color: Colors.white,
    },
    title: {
        fontSize: 30,
        color: Colors.white,
        letterSpacing: 2
    },
    place: {
        color: Colors.white
    },
    icon: {
        fontSize: 12,
        color: Colors.white,
        paddingRight: 3,
        marginLeft: 0,
        marginRight: 0
    }
  });

export default Ticket;
