import React, { useCallback, useEffect } from 'react';
import { View, ScrollView, Text, Image, StatusBar } from 'react-native';
import { Tab, Tabs } from 'native-base';
import { useSelector, useDispatch } from 'react-redux';
import Colors from 'helpers/Colors';
import { H2 } from 'native-base';
import styles from './styles';

import TextStyles from 'helpers/TextStyles';
import Ticket from './Ticket';
import strings from 'localization';
import getUser from 'selectors/UserSelectors';


const data = [
  {
    id: 1,
    title: 'Tomorrowland',
    image: 'https://www.tomorrowland.com/src/Frontend/Themes/tomorrowland/Core/Layout/images/timeline/2017-1.jpg',
    month: 'October',
    day: 14,
    location: 'Belgium',
    place: 'De Schorre',
    year: 2019,
    about: 'Tomorrowland is an electronic dance music festival held in Boom, Belgium. Tomorrowland was first held in 2005 and has since become one of the world\'s...'
  },
  {
    id: 2,
    title: 'Veld',
    image: 'https://media.blogto.com/articles/veld-new.JPG?w=2048&cmd=resize_then_crop&height=1365&quality=70',
    month: 'November',
    day: 7,
    location: 'Toronto',
    place: 'Downsview Park',
    year: 2019,
    about: 'VELD Music Festival is an annual electronic dance music and hip-hop music festival held at Downsview Park in Toronto, Ontario. Since its inception in 2012...',
  },
  {
    id: 3,
    title: 'Coachella',
    image: 'https://consequenceofsound.net/wp-content/uploads/2019/04/Coachella-2019-YouTube-STreaming-Schedule-e1555079789554.png?w=800',
    month: 'April',
    day: 19,
    location: 'California',
    place: 'Empire Polo Club',
    year: 2020,
    about: 'The Coachella Valley Music and Arts Festival is an annual music and arts festival held at the Empire Polo Club in Indio, California, in the Coachella Valley...',
  },
  {
    id: 4,
    title: 'Meadows in the Mountains',
    image: 'https://d49r1np2lhhxv.cloudfront.net/image/1400x1050/center/middle/filters:quality(70)/www/admin/uploads/images/Top50MusicFestivalsintheWorld_MINM.jpg',
    month: 'June',
    day: 4,
    location: 'Bulgaria',
    place: 'Polkovnik',
    year: 2019,
    about: 'The monastery bell chimes to the 10th chapter of Meadows In The Mountains Festival and what an epic 10 years it has been. Another time we will tell the story...',
  }
];

function Wallet(props) {
  const user = useSelector(state => getUser(state));
  const openTicket = (id) => props.navigation.navigate('TicketInfoScreen', {data: data[id-1]});
  // props.navigation.navigate('TicketInfoScreen', {data: data[0]});
  useEffect(() => {
    if (user === null) {
      props.navigation.navigate('Auth');
    }
  });

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      {/* <Tabs tabBarUnderlineStyle={{backgroundColor: Colors.primary}} locked>
        <Tab heading={strings.wallet.tab1}> */}
          <ScrollView style={styles.ticketContainer}>
            {data.map(ticket => {
              return <Ticket onPress={openTicket} key={ticket.id} options={ticket}/>
            })}
          </ScrollView>
        {/* </Tab>
        <Tab heading={strings.wallet.tab2}>
          <Text></Text>
        </Tab>
      </Tabs> */}
    </View>
  );
}

Wallet.navigationOptions = {
  title: strings.wallet.title
};

export default Wallet;
