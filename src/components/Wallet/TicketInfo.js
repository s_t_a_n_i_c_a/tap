import React, { useEffect, useState } from 'react';
import { View, ScrollView, Text, Image, StyleSheet, Alert, Modal } from 'react-native';
import { Icon, Button, H2 } from 'native-base';
import { useSelector } from 'react-redux';
import Colors from 'helpers/Colors';
import strings from 'localization';
import getUser from 'selectors/UserSelectors';
import Biometrics from 'react-native-biometrics'
import { Overlay } from 'react-native-elements';
import QRCode from 'react-native-qrcode-svg';
import { RNCamera } from 'react-native-camera';
import DialogInput from 'react-native-dialog-input';
import TopUp from './TopUp';

function TicketInfo(props) {
    const user = useSelector(state => getUser(state));
    const data = props.navigation.getParam('data');
    const [ticketModalVisible, setTicketModalVisible] = useState(false);
    const [wristbands, updateWristbands] = useState([]);
    const [wristbandModalVisible, setWristbandModalVisible] = useState(false);
    const [cameraModalVisible, setCameraModalVisible] = useState(false);
    const [dialogVisible, setDialogVisible] = useState(false);
    const [funds, setFunds] = useState(0);
    const [topUpModalVisible, setTopUpModalVisible] = useState(false);

    const checkBiometrics = () => {
        Biometrics.simplePrompt('Confirm identity')
        .then(() => {
            setTicketModalVisible(true);
        })
        .catch(() => {
            Alert.alert(
                'Authentication Required',
                'You need to authenticate yourself before viewing your ticket.',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
        })
    }
    
    useEffect(() => {
        if (user === null) {
        props.navigation.navigate('Auth');
        }
    });

    return (
        <ScrollView style={styles.root}>
            <Image style={styles.image} source={{uri: data.image}}/>
            <TopUp topUpModalVisible={topUpModalVisible} setTopUpModalVisible={setTopUpModalVisible} funds={funds} setFunds={setFunds}/>
            <Overlay isVisible={ticketModalVisible} overlayStyle={styles.ticketModal} onBackdropPress={() => setTicketModalVisible(false)}>
                <View style={{alignItems: 'center'}}>
                    <QRCode
                        value="Rob was here 2019 wussup"
                        color={Colors.black}
                        size={200}
                    />
                </View>
            </Overlay>
            <DialogInput isDialogVisible={dialogVisible}
                title={"Name"}
                message={"Enter the name of the wristband owner"}
                hintInput ={"Name"}
                submitInput={ (inputText) => {
                    let users = wristbands;
                    users.push(inputText);
                    updateWristbands(users);
                    setDialogVisible(false);
                }}
                closeDialog={ () => {setDialogVisible(false)}}>
            </DialogInput>
            <Modal
                animationType="slide"
                transparent={false}
                visible={cameraModalVisible}>
                <RNCamera
                    style={{display: 'flex', flex:1}}
                    defaultTouchToFocus
                    onBarCodeRead={(t) => {
                        setCameraModalVisible(false);
                        setDialogVisible(true);
                    }}
                    type={RNCamera.Constants.Type.back}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    >
                    {({ camera, status }) => {
                        if(status !== 'READY') return (
                            <View
                                style={{
                                flex: 1,
                                backgroundColor: 'white',
                                justifyContent: 'center',
                                alignItems: 'center',
                                }}
                            >
                                <Text>Waiting</Text>
                            </View>
                            )
                        return (
                        <View style={styles.buttonContainer}>
                            <Button transparent
                                onPress={() => {
                                    setCameraModalVisible(false);
                                    setDialogVisible(false)
                                }}
                                style={styles.capture}>
                                <Icon style={styles.cameraIcon} active name="close" type='EvilIcons'></Icon>
                            </Button>
                        </View>
                        );
                    }}
                </RNCamera>
            </Modal>
            <Overlay isVisible={wristbandModalVisible} overlayStyle={styles.wristbandModal} onBackdropPress={() => setWristbandModalVisible(false)}>
                <View>
                    <H2 style={{textAlign: 'left', color: Colors.black, marginBottom: 20, marginTop: 10}}>Link a wristband</H2>
                    <Text style={{color: Colors.muted}}>To link a wristband, choose from one of the options below. You can use your phone
                        to scan the wristband or enter the code manually.
                    </Text>
                    <View style={{marginTop: 10}}>
                        <Button transparent style={{justifyContent:'center'}}><Text style={{fontSize: 18, color: Colors.primary, textAlign: 'center'}}>NFC Scan</Text></Button>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{backgroundColor: Colors.gray, height: 1, flex: 1, alignSelf: 'center'}} />
                            <Text style={{ color: Colors.gray, alignSelf:'center', paddingHorizontal:5, fontSize: 15 }}>Or</Text>
                            <View style={{backgroundColor: Colors.gray, height: 1, flex: 1, alignSelf: 'center'}} />
                        </View>
                        <Button transparent onPress={() => {setWristbandModalVisible(false); setCameraModalVisible(true)}} style={{justifyContent:'center'}}><Text style={{fontSize: 18, color: Colors.primary, textAlign: 'center'}}>QR Code Scan</Text></Button>
                        <Button transparent onPress={() => {}} style={{marginTop: 20, justifyContent:'flex-end', alignItems: 'flex-end'}}>
                            <Text style={{fontSize: 14, color: Colors.hero, textAlign: 'right'}}>Manual entry</Text>
                            <Icon style={{marginRight: 0, marginLeft: 0, color: Colors.hero, fontSize: 13}} type="Entypo" name="chevron-right"/>
                        </Button>
                    </View>
                </View>
            </Overlay>
            <View style={styles.overlay}>
                <Text style={styles.title}>{data.title}</Text>
                <Text style={styles.date}>{data.month + ' ' + data.day + ', ' + data.year}</Text>
                <View style={styles.eventInfoContainer}>
                    <View style={styles.leftInfo}>
                        <Icon style={[styles.icon, {paddingRight:5}]} type='Entypo' name='location-pin'/>
                        <Text style={styles.location}>{data.location}</Text>
                    </View>
                    <View style={styles.rightInfo}>
                        <Icon style={[styles.icon, {paddingRight:8}]} type='Entypo' name='home'/>
                        <Text style={styles.location}>{data.place}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.body}>
                <View style={styles.sectionContainer}>
                    <Button full onPress={() => checkBiometrics()} style={styles.viewTicketButton}>
                        <Icon style={[styles.icon, {marginLeft: 0}]} type='FontAwesome5' name='fingerprint'/>
                        <Text style={styles.viewTicketText}>VIEW TICKET</Text>
                    </Button>
                </View>
                <View style={styles.sectionContainer}>
                    <Text style={styles.sectionTitle}>About</Text>
                    <Text style={styles.sectionText}>{data.about}</Text>
                    <Text style={styles.readMore}>Read more ></Text>
                </View>
                <View style={styles.sectionContainer}>
                    <View style={styles.wristbandHeader}>
                        <Text style={styles.sectionTitle}>Wristbands linked to your account</Text>
                        <Button onPress={() => setWristbandModalVisible(true)} transparent style={styles.wristbandAddButton}><Icon style={styles.wristbandAddIcon} type='Entypo' name='circle-with-plus'/></Button>
                    </View>
                    { wristbands.length === 0 ?
                        <Text style={{fontStyle:'italic'}}>No wristbands associated</Text>
                        :
                        wristbands.map((person, i) => { 
                            return <View key={person} style={[styles.wristbandHeader, {height: 30}]}>
                                <Text style={[styles.sectionText, {marginTop: 0}]}>{person}</Text>
                                <Button onPress={() => {
                                    let users;
                                    console.log(wristbands.indexOf(person))
                                    users = wristbands.filter(user => user !== person)
                                    updateWristbands(users);
                                }} transparent style={styles.wristbandAddButton}><Icon style={[styles.wristbandAddIcon, {color: Colors.error}]} type='Entypo' name='circle-with-minus'/></Button>
                            </View>
                        })
                    }
                </View>
                <View style={styles.sectionContainer}>
                    <View style={styles.wristbandHeader}>
                        <Text style={styles.sectionTitle}>Add funds</Text>
                        <Button onPress={() => setTopUpModalVisible(true)} transparent style={styles.wristbandAddButton}><Icon style={styles.wristbandAddIcon} type='Entypo' name='circle-with-plus'/></Button>
                    </View>
                    <Text>Funds available: <Text style={{fontWeight:'bold', color: Colors.alternative}}>${funds}</Text></Text>
                </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    root: {
      flex: 1
    },
    image: {
        height: 250,
        width: '100%',
    },
    overlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        height: 250,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 30,
        color: Colors.white,
        fontWeight: 'bold'
    },
    date: {
        fontSize: 14,
        color: Colors.white,
        marginTop: 5,
    },
    eventInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingBottom: 10
    },
    leftInfo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    rightInfo: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    location: {
        color: Colors.white,
    },
    icon: {
        color: Colors.white,
        fontSize: 18,
    },
    body: {
        padding: 25,
        backgroundColor: Colors.white
    },
    sectionContainer: {
        marginBottom: 40,
    },
    sectionTitle: {
        color: Colors.muted,
        fontSize: 13,
        fontWeight: 'bold'
    },
    sectionText: {
        color: Colors.black,
        fontSize: 16,
        marginTop: 10,
        lineHeight: 24
    },
    readMore: {
        color: Colors.hero,
        textDecorationLine: 'underline',
        marginTop: 5
    },
    ticketModal: {
        height: 'auto',
        width: 'auto'
    },
    viewTicketButton: {
        backgroundColor: Colors.alternative,
    },
    viewTicketText: {
        color: Colors.white,
        fontWeight: 'bold'
    },
    wristbandHeader: {
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems: 'center',
    },
    wristbandAddIcon: {
        color: Colors.hero,
        fontSize: 24,
        marginRight: 0,
        marginLeft: 0
    },
    wristbandModal: {
        height: 'auto',
        width: '90%',
        padding: 20
    },
    buttonContainer: {
        position: 'absolute',
        width: '100%',
        top: 40,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 20
    },
    capture: {
        flex: 0,
        backgroundColor: 'transparent',
        height: 'auto',
        justifyContent: 'center'
    },
    cameraIcon: {
        fontSize: 40,
        color: 'rgba(255,255,255,0.8)'
    }
});

TicketInfo.navigationOptions = {
    headerStyle: {
        
    }
  };

export default TicketInfo;
