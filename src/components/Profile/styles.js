import { StyleSheet } from 'react-native';
import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.white,
    marginTop: 20,
  },
  milanContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  milan: {
    borderColor: Colors.lightGray,
    borderWidth: 2,
  },
  milanName: {
    color: Colors.primary,
    fontSize: 24,
    textAlign: 'center',
  },
  profileContainer: {
    flex: 1,
    marginTop: 20,
  },
  profileItem: {
    paddingTop: 20,
    paddingHorizontal: 20,
    alignItems: 'center',
    flex: 1,
    backgroundColor: Colors.lightGray
  },
  cardInfoRow: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    marginBottom: 20
  },
  cardImage: {
      width: 70,
      height: 50,
      resizeMode: 'contain'
  },
  viewTicketButton: {
    backgroundColor: Colors.alternative,
    paddingHorizontal: 30
  },
  viewTicketText: {
    color: Colors.white,
    fontWeight: 'bold'
  },
  plusIcon: {
    fontSize: 30,
    marginRight: 10,
    marginLeft: 0,
  },
  profileDetailsContainer: {
    alignSelf: 'stretch',
    flex: 1,
    marginBottom: 20,
    shadowColor: 'rgb(58,55,55)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    elevation: 3,
  },
  profileDetails: {
    flexDirection: 'row',
  }
});

export default styles;
