import React, { useCallback, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  Switch, Image
} from 'react-native';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { Thumbnail, Tab, Tabs, Icon, Button, Content, Card, CardItem, Body} from 'native-base';
import styles from './styles';

import strings from 'localization';
import { logout } from 'actions/UserActions';
import getUser from 'selectors/UserSelectors';
import carlo from 'assets/images/carlo.jpg';
import Colors from 'helpers/Colors';

function Profile(props) {
  const user = useSelector(state => getUser(state));
  const dispatch = useDispatch();
  const logoutUser = useCallback(() => {  props.navigation.navigate('Onboarding'); dispatch(logout()), [dispatch] });

  useEffect(() => {
    if (user === null) {
      props.navigation.navigate('Auth');
    }
  });

  return (
    <View style={styles.container}>
      <View style={styles.milanContainer}>
        <View style={{width:100}}>
          <Thumbnail style={styles.milan} large source={carlo}/>
        </View>
        <View style={{alignItems: 'flex-start'}}>
          <Text style={styles.milanName}>{user.name}</Text>
          <Text style={{color: Colors.black, paddingTop: 2}}>{user.details.email}</Text>
          <Text style={{color: Colors.muted, paddingTop: 4, fontSize: 13}}>{user.details.phone}</Text>
        </View>
      </View>
      <View style={styles.profileContainer}>
        <Tabs>
          <Tab heading="Profiles">
            <ScrollView style={{backgroundColor: Colors.lightGray}}>
              <View style={styles.profileItem}>
                { user.profiles.map( (profile, id) => 
                  <Card key={id} style={styles.profileDetailsContainer} transparent>
                    <CardItem>
                      <Button transparent iconLeft small style={{position: 'absolute', top:0, right: 5,}}><Icon style={{fontSize: 16, color: Colors.primary}} type="FontAwesome5" name="edit"/></Button>
                      <Body style={{paddingVertical:15}}>
                        <View style={styles.profileDetails}>
                          <View style={{flex: 1}}>
                            <Thumbnail style={styles.profilePicture} square large source={{uri:'https://api.adorable.io/avatars/' + Math.floor(Math.random() * 200)}}/>
                          </View>
                          <View style={{flex: 2}}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                              <Text style={{fontSize: 16, color: Colors.black,}}>{profile.profile_name}</Text>
                            </View>
                            <Text style={{fontSize: 20, color: Colors.hero,  fontWeight: 'bold'}}>{profile.user_name}</Text>
                            <View style={{flexDirection: 'row', justifyContent:'flex-start', alignItems:'flex-end'}}>
                              <Icon style={{fontSize: 13, color: Colors.muted, marginRight: 5}} type="Entypo" name="mail"/>
                              <Text style={{color: Colors.muted}}>{profile.email}</Text>
                            </View>
                            <View style={{flexDirection: 'row', justifyContent:'flex-start', alignItems:'flex-end'}}>
                              <Icon style={{fontSize: 13, color: Colors.muted, marginRight: 5}} type="Entypo" name="phone"/>
                              <Text style={{fontSize: 13, color: Colors.muted}}>{profile.phone}</Text>
                            </View>
                          </View>
                        </View>
                        <Text style={{marginTop: 20, color: Colors.muted}}>{profile.description}</Text>
                      </Body>
                    </CardItem>
                  </Card>
                )}
                <View style={{flex: 1,justifyContent: 'flex-end', marginBottom: 20}}>
                  <Button full onPress={logoutUser} style={styles.viewTicketButton}>
                      <Icon style={[styles.plusIcon, {marginLeft: 0}]} type='EvilIcons' name='arrow-right'/>
                      <Text style={styles.viewTicketText}>{strings.logout}</Text>
                  </Button>
                </View>
              </View>
            </ScrollView>
          </Tab>
          <Tab heading="Cards">
            <View style={styles.profileItem}>
              <View style={styles.cardInfoRow}>
                  <Image style={styles.cardImage} source={{uri: 'https://motuscc.com/wp-content/uploads/2017/04/new-mastercard-2017.png'}}/>
                  <View style={{marginLeft: 20}}>
                    <Text style={{color:Colors.muted}}>CREDIT CARD</Text>
                    <Text style={{fontSize: 14, color: Colors.muted}}>XXXX XXXX XXXX 5678</Text>
                  </View>
                  <View style={{marginLeft: 20, flex:1, alignItems: 'flex-end'}}>
                    <Icon style={{fontSize: 24, color: Colors.error}} type="Entypo" name="circle-with-minus"/>
                  </View>
              </View>
              <View style={styles.cardInfoRow}>
                  <Image style={styles.cardImage} source={{uri: 'https://www.yesbank.in/squareimages/YES%20Premia%20Debit%20Card_1027x656.png'}}/>
                  <View style={{marginLeft: 20}}>
                    <Text style={{color:Colors.muted}}>DEBIT CARD</Text>
                    <Text style={{fontSize: 14, color: Colors.muted}}>XXXX XXXX XXXX 1234</Text>
                  </View>
                  <View style={{marginLeft: 20, flex:1, alignItems: 'flex-end'}}>
                    <Icon style={{fontSize: 24, color: Colors.error}} type="Entypo" name="circle-with-minus"/>
                  </View>
              </View>
              <View style={{flex: 1,justifyContent: 'flex-end', marginBottom: 20}}>
                <Button full onPress={() => {}} style={styles.viewTicketButton}>
                    <Icon style={[styles.plusIcon, {marginLeft: 0}]} type='EvilIcons' name='plus'/>
                    <Text style={styles.viewTicketText}>Add new card</Text>
                </Button>
              </View>
            </View>
          </Tab>
          {/* <Tab heading="Orders">
            <Text>asd</Text>
          </Tab> */}
        </Tabs>
      </View>
    </View>
  );
}

Profile.navigationOptions = {
  title: strings.profile,
};

Profile.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Profile;
