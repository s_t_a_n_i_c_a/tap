import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Svg, { Path } from "react-native-svg";

export default class Page2 extends Component {
  render() {
    return (
      <View style={styles.root}>
        <Text style={styles.diversifyRechargeA}>
          Diversify recharge and withdraw money, free recharge with bank account
        </Text>
        <Text style={styles.easyDepositAndWit}>
          Easy Deposit and Withdrawal
        </Text>
        <View style={styles.icon}>
          <View style={styles.style}>
            <Svg viewBox={"-0 -0 160 160"} style={styles.oval}>
              <Path
                strokeWidth={0}
                fill={"rgba(255,255,255,1)"}
                d={
                  "M80.00 160.00 C124.18 160.00 160.00 124.18 160.00 80.00 C160.00 35.82 124.18 0.00 80.00 0.00 C35.82 0.00 0.00 35.82 0.00 80.00 C0.00 124.18 35.82 160.00 80.00 160.00 C124.18 160.00 80.00 160.00 80.00 160.00 Z"
                }
              />
            </Svg>
            <View style={styles.money}>
              <Svg
                viewBox={"-0.5 -0.5 33.30434782608696 48.95652173913044"}
                style={styles.stroke1}
              >
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(9,32,88,1)"}
                  d={
                    "M30.40 7.18 C29.81 5.78 28.97 4.51 27.93 3.45 C26.89 2.38 25.65 1.52 24.28 0.93 C22.91 0.33 21.40 0.00 19.82 0.00 L17.74 0.00 L15.65 0.00 L13.57 0.00 L11.48 0.00 C9.90 0.00 8.39 0.33 7.02 0.93 C5.65 1.52 4.41 2.38 3.37 3.45 C2.33 4.51 1.49 5.78 0.91 7.18 C0.32 8.59 0.00 10.12 0.00 11.74 C0.00 13.35 0.32 14.90 0.91 16.30 C1.49 17.70 2.33 18.97 3.37 20.03 C4.41 21.09 5.65 21.96 7.02 22.55 C8.39 23.15 9.90 23.48 11.48 23.48 L13.57 23.48 L15.65 23.48 L17.74 23.48 L19.82 23.48 C21.40 23.48 22.91 23.81 24.28 24.40 C25.65 25.00 26.89 25.86 27.93 26.93 C28.97 27.99 29.81 29.26 30.40 30.66 C30.98 32.06 31.30 33.60 31.30 35.22 C31.30 36.83 30.98 38.37 30.40 39.77 C29.81 41.18 28.97 42.45 27.93 43.51 C26.89 44.57 25.65 45.43 24.28 46.03 C22.91 46.63 21.40 46.96 19.82 46.96 L17.74 46.96 L15.65 46.96 L13.57 46.96 L11.48 46.96 C9.90 46.96 8.39 46.63 7.02 46.03 C5.65 45.43 4.41 44.57 3.37 43.51 C2.33 42.45 1.49 41.18 0.91 39.77 C29.81 5.78 30.40 7.18 30.40 7.18 Z"
                  }
                />
              </Svg>
              <Svg
                viewBox={"-1 -1 6.608695652173912 61.39130434782609"}
                style={styles.stroke3}
              >
                <Path
                  strokeWidth={2}
                  fill={"transparent"}
                  stroke={"rgba(184,233,134,1)"}
                  d={
                    "M2.30 1.00 C2.30 58.39 2.30 58.39 2.30 58.39 L2.30 1.00 Z"
                  }
                />
              </Svg>
            </View>
          </View>
          <View style={styles.style1}>
            <Svg viewBox={"-0 -0 80 80"} style={styles.oval1}>
              <Path
                strokeWidth={0}
                fill={"rgba(255,255,255,1)"}
                d={
                  "M40.00 80.00 C62.09 80.00 80.00 62.09 80.00 40.00 C80.00 17.91 62.09 0.00 40.00 0.00 C17.91 0.00 0.00 17.91 0.00 40.00 C0.00 62.09 17.91 80.00 40.00 80.00 C62.09 80.00 40.00 80.00 40.00 80.00 Z"
                }
              />
            </Svg>
            <Svg
              viewBox={"-0.5 -0.5 15.33333333333333 23.66666666666667"}
              style={styles.stroke5}
            >
              <Path
                strokeWidth={1}
                fill={"transparent"}
                stroke={"rgba(0,239,224,1)"}
                d={
                  "M13.33 8.33 C6.67 0.00 6.67 0.00 6.67 0.00 L0.00 8.33 L3.52 8.33 L5.00 21.67 L8.33 21.67 L9.81 8.33 L13.33 8.33 L13.33 8.33 Z"
                }
              />
            </Svg>
          </View>
          <View style={styles.style2}>
            <Svg viewBox={"-0 -0 60 60"} style={styles.oval2}>
              <Path
                strokeWidth={0}
                fill={"rgba(255,255,255,1)"}
                d={
                  "M30.00 60.00 C46.57 60.00 60.00 46.57 60.00 30.00 C60.00 13.43 46.57 0.00 30.00 0.00 C13.43 0.00 0.00 13.43 0.00 30.00 C0.00 46.57 13.43 60.00 30.00 60.00 C46.57 60.00 30.00 60.00 30.00 60.00 Z"
                }
              />
            </Svg>
            <Svg
              viewBox={"-0.5 -0.5 15.33333333333334 23.66666666666667"}
              style={styles.stroke7}
            >
              <Path
                strokeWidth={1}
                fill={"transparent"}
                stroke={"rgba(0,200,249,1)"}
                d={
                  "M0.00 13.33 C6.67 21.67 6.67 21.67 6.67 21.67 L13.33 13.33 L9.81 13.33 L8.33 0.00 L5.00 0.00 L3.52 13.33 L0.00 13.33 L0.00 13.33 Z"
                }
              />
            </Svg>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)"
  },
  diversifyRechargeA: {
    top: "80.11%",
    left: "0.00%",
    width: "100.00%",
    height: 72,
    backgroundColor: "transparent",
    color: "rgba(79,108,141,1)",
    position: "absolute",
    fontSize: 17,
    textAlign: "center"
  },
  easyDepositAndWit: {
    top: "58.56%",
    left: "18.46%",
    backgroundColor: "transparent",
    color: "rgba(9,32,88,1)",
    position: "absolute",
    fontSize: 22,
    textAlign: "center"
  },
  icon: {
    top: "0.00%",
    left: "13.98%",
    width: "68.46%",
    height: "49.72%",
    position: "absolute"
  },
  style: {
    top: "11.11%",
    left: "10.73%",
    width: "83.77%",
    height: "88.89%",
    position: "absolute"
  },
  oval: {
    top: "0.00%",
    left: "0.00%",
    width: "100.00%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  money: {
    top: "31.88%",
    left: "40.03%",
    width: "20.00%",
    height: "36.25%",
    position: "absolute"
  },
  stroke1: {
    top: "8.66%",
    left: "-0.61%",
    width: "104.08%",
    height: "84.41%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke3: {
    top: "-1.20%",
    left: "42.66%",
    width: "20.65%",
    height: "105.85%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  style1: {
    top: "0.00%",
    left: "0.26%",
    width: "41.88%",
    height: "44.44%",
    position: "absolute"
  },
  oval1: {
    top: "0.00%",
    left: "0.00%",
    width: "100.00%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke5: {
    top: "35.83%",
    left: "41.04%",
    width: "19.17%",
    height: "29.58%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  style2: {
    top: "61.11%",
    left: "68.32%",
    width: "31.41%",
    height: "33.33%",
    position: "absolute"
  },
  oval2: {
    top: "0.00%",
    left: "0.00%",
    width: "100.00%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke7: {
    top: "31.11%",
    left: "38.06%",
    width: "25.56%",
    height: "39.44%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  }
});
