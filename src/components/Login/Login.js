import React, { useCallback, useEffect, useState } from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  NativeModules
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import ErrorView from '../common/ErrorView';
import styles from './styles';
import logo from 'assets/images/logo_black.png';

import getUser from 'selectors/UserSelectors';
import errorsSelector from 'selectors/ErrorSelectors';
import { isLoadingSelector } from 'selectors/StatusSelectors';
import strings from 'localization';
import { login, actionTypes } from 'actions/UserActions';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Button, Icon, Item, Input} from 'native-base';
// import Mitek from 'react-native-mitek';

function Login(props) {
  const [email, setEmail] = useState('robert.stanica@intellitix.com');
  const [password, setPassword] = useState('Matt123!');

  const user = useSelector(state => getUser(state));
  const isLoading = useSelector(state => isLoadingSelector([actionTypes.LOGIN], state));
  const errors = useSelector(state => errorsSelector([actionTypes.LOGIN, actionTypes.PROFILE], state));

  const dispatch = useDispatch();
  const loginUser = useCallback(() => dispatch(login(email, password)), [email, password, dispatch]);
  const passwordChanged = useCallback(value => setPassword(value), []);
  const emailChanged = useCallback(value => setEmail(value), []);

  var mitek = NativeModules.Mitek;
  
  console.log(mitek.sampleMethod('antoine',19, (data) => console.log(data)))
// CalendarManager.addEvent('Birthday Party', '4 Privet Drive, Surrey');

  useEffect(() => {
    if (user !== null) {
      props.navigation.navigate('Home');
    }
  });

  return (
    <View style={styles.container}>
      <StatusBar  hidden/>
      <View style={styles.formContainer}>
        <View style={styles.header}>
          <Image source={logo} style={styles.logo}/>
        </View>
        <View style={{flex:1}}>
          <Item style={styles.formField}>
            <Icon active name='mail' style={styles.loginIcon}/>
            <Input
              style={styles.textField}
              placeholder={strings.email}
              placeholderTextColor="rgba(45,95,128,1)"
              onChangeText={emailChanged}
              value={email}/>
          </Item>
          <Item style={styles.formField}>
            <Icon active name='lock' style={styles.loginIcon}/>
            <Input
              style={styles.textField}
              placeholder={strings.password}
              placeholderTextColor = "rgba(45,95,128,1)"
              value={password}
              onChangeText={passwordChanged}
              secureTextEntry/>
          </Item>
          <ErrorView errors={errors} />
          <View style={styles.extraButtonsContainer}>
            <Button transparent light onPress={() => props.navigation.navigate('Register')}>
              <Text style={styles.extraButtons}>Create Account</Text>
            </Button>
            <Button transparent light>
              <Text style={styles.extraButtons}>Forgot Password</Text>
            </Button>
          </View>
        </View>
      </View>
      <View style={styles.loginContainer}>
        <Button
          block
          onPress={loginUser}
          style={styles.loginButton}
        >
          <Text style={styles.loginButtonText}> {isLoading ? strings.loading : strings.login}</Text>
        </Button>
      </View>
    </View>
  );
}

Login.navigationOptions = {
  headerStyle: {
    margin: Platform.OS === 'ios' ? -getStatusBarHeight() :  -getStatusBarHeight() - 4,
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
    elevation: 0
  },
  // headerTitle: <Thumbnail style={styles.headerImage} source={{uri: 'https://iukl.edu.my/wp-content/uploads/2018/01/bachelor-of-communication-hons-in-corporate-communication-1.jpg'}} square/>
};

Login.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Login;
