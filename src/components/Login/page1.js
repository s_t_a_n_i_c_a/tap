import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import Svg, { Path, SvgImage } from "react-native-svg";

export default class Page1 extends Component {
  render() {
    return (
      <View style={styles.root}>
        <Text style={styles.multiStoreySecurit}>
          Multi-story security, international standard
        </Text>
        <Text style={styles.absoluteSafety}>Absolute Safety</Text>
        <View style={styles.icon}>
          <View style={styles.style5}>
            <Svg viewBox={"-0 -0 160 160"} style={styles.oval}>
              <Path
                strokeWidth={0}
                fill={"rgba(255,255,255,1)"}
                d={
                  "M80.00 160.00 C124.18 160.00 160.00 124.18 160.00 80.00 C160.00 35.82 124.18 0.00 80.00 0.00 C35.82 0.00 0.00 35.82 0.00 80.00 C0.00 124.18 35.82 160.00 80.00 160.00 C124.18 160.00 80.00 160.00 80.00 160.00 Z"
                }
              />
            </Svg>
            <View style={styles.group7}>
              <Svg
                viewBox={"-0.5 -0.5 61.24999999999999 7.134999999999995"}
                style={styles.stroke1}
              >
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(9,32,88,1)"}
                  d={
                    "M0.00 5.13 C17.18 -1.71 42.07 -1.71 59.25 5.13 C17.18 -1.71 0.00 5.13 0.00 5.13 Z"
                  }
                />
              </Svg>
              <Svg
                viewBox={"-0.5 -0.5 61.24999999999999 72.57333333333334"}
                style={styles.stroke3}
              >
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(9,32,88,1)"}
                  d={
                    "M59.25 0.00 C59.25 30.88 59.25 30.88 59.25 30.88 C59.25 45.23 51.48 58.43 38.94 65.41 L29.63 70.57 L20.31 65.41 C7.77 58.43 0.00 45.23 0.00 30.88 L0.00 0.00 L59.25 0.00 Z"
                  }
                />
              </Svg>
              <Svg
                viewBox={"-1 -1 33.625 27.04166666666666"}
                style={styles.stroke5}
              >
                <Path
                  strokeWidth={2}
                  fill={"transparent"}
                  stroke={"rgba(184,233,134,1)"}
                  d={
                    "M30.63 1.00 C10.88 24.04 10.88 24.04 10.88 24.04 L1.00 14.17 L30.63 1.00 Z"
                  }
                />
              </Svg>
            </View>
          </View>
          <View style={styles.style6}>
            <Svg viewBox={"-0 -0 60 60"} style={styles.oval1}>
              <Path
                strokeWidth={0}
                fill={"rgba(255,255,255,1)"}
                d={
                  "M30.00 60.00 C46.57 60.00 60.00 46.57 60.00 30.00 C60.00 13.43 46.57 0.00 30.00 0.00 C13.43 0.00 0.00 13.43 0.00 30.00 C0.00 46.57 13.43 60.00 30.00 60.00 C46.57 60.00 30.00 60.00 30.00 60.00 Z"
                }
              />
            </Svg>
            <View style={styles.group11}>
              <Svg viewBox={"-0.5 -0.5 25.75 17"} style={styles.stroke11}>
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(9,32,88,1)"}
                  d={
                    "M22.50 15.00 C1.25 15.00 1.25 15.00 1.25 15.00 C0.56 15.00 0.00 14.44 0.00 13.75 L0.00 1.25 C0.00 0.56 0.56 0.00 1.25 0.00 L22.50 0.00 C23.19 0.00 23.75 0.56 23.75 1.25 L23.75 13.75 C23.75 14.44 23.19 15.00 22.50 15.00 L22.50 15.00 Z"
                  }
                />
              </Svg>
              <Svg viewBox={"-0.5 -0.5 18.25 15.75"} style={styles.stroke31}>
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(9,32,88,1)"}
                  d={
                    "M8.13 0.00 C8.13 0.00 8.13 0.00 8.13 0.00 C3.64 0.00 0.00 3.64 0.00 8.13 L0.00 13.75 L16.25 13.75 L16.25 8.13 C16.25 3.64 12.61 0.00 8.13 0.00 L8.13 0.00 Z"
                  }
                />
              </Svg>
              <Svg
                viewBox={"-0.5 -0.5 10.750000000000002 12"}
                style={styles.stroke51}
              >
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(9,32,88,1)"}
                  d={
                    "M0.00 10.00 C0.00 4.43 0.00 4.43 0.00 4.43 C0.00 1.99 1.96 0.00 4.38 0.00 C6.79 0.00 8.75 1.99 8.75 4.43 L8.75 10.00 L0.00 10.00 L0.00 10.00 Z"
                  }
                />
              </Svg>
              <Svg
                viewBox={"-0.5 -0.5 5.7500000000000036 5.7500000000000036"}
                style={styles.stroke7}
              >
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(47,97,213,1)"}
                  d={
                    "M3.75 1.88 C3.75 2.91 2.91 3.75 1.88 3.75 C0.84 3.75 0.00 2.91 0.00 1.88 C0.00 0.84 0.84 0.00 1.88 0.00 C2.91 0.00 3.75 0.84 3.75 1.88 C3.75 2.91 3.75 1.88 3.75 1.88 Z"
                  }
                />
              </Svg>
              <Svg
                viewBox={"-0.5 -0.5 5.7500000000000036 6.078749999999999"}
                style={styles.stroke9}
              >
                <Path
                  strokeWidth={1}
                  fill={"transparent"}
                  stroke={"rgba(47,97,213,1)"}
                  d={
                    "M1.88 0.33 C1.48 0.33 1.12 0.21 0.82 0.00 L0.00 4.08 L3.75 4.08 L2.93 0.00 C2.63 0.21 2.27 0.33 1.88 0.33 C1.48 0.33 1.88 0.33 1.88 0.33 Z"
                  }
                />
              </Svg>
            </View>
          </View>
          <View style={styles.style7}>
            <Svg viewBox={"-0 -0 80 80"} style={styles.oval2}>
              <Path
                strokeWidth={0}
                fill={"rgba(255,255,255,1)"}
                d={
                  "M40.00 80.00 C62.09 80.00 80.00 62.09 80.00 40.00 C80.00 17.91 62.09 0.00 40.00 0.00 C17.91 0.00 0.00 17.91 0.00 40.00 C0.00 62.09 17.91 80.00 40.00 80.00 C62.09 80.00 40.00 80.00 40.00 80.00 Z"
                }
              />
            </Svg>
            <View style={styles.group2}>
              <Svg
                viewBox={"-0 -0 24.02666666666669 9.626666666666672"}
                style={styles.path}
              >
                <Path
                  strokeWidth={0}
                  fill={"rgba(0,239,224,1)"}
                  d={
                    "M4.63 0.00 C5.50 0.00 6.28 0.20 6.98 0.61 C7.68 1.02 8.23 1.59 8.63 2.33 C9.03 3.06 9.23 3.89 9.23 4.81 C9.23 5.74 9.03 6.57 8.63 7.30 C8.23 8.03 7.68 8.60 6.98 9.01 C6.28 9.42 5.50 9.63 4.63 9.63 C3.76 9.63 2.97 9.42 2.26 9.01 C1.55 8.60 1.00 8.03 0.60 7.30 C0.20 6.57 0.00 5.74 0.00 4.81 C0.00 3.89 0.20 3.06 0.60 2.33 C1.00 1.59 1.55 1.02 2.26 0.61 C2.97 0.20 3.76 0.00 4.63 0.00 C5.50 0.00 4.63 0.00 4.63 0.00 Z M4.63 1.13 C3.98 1.13 3.40 1.28 2.89 1.58 C2.38 1.88 1.98 2.30 1.69 2.86 C1.40 3.42 1.25 4.07 1.25 4.81 C1.25 5.55 1.40 6.20 1.69 6.76 C1.98 7.32 2.38 7.75 2.89 8.05 C3.40 8.34 3.98 8.49 4.63 8.49 C5.27 8.49 5.84 8.34 6.35 8.04 C6.86 7.74 7.26 7.31 7.55 6.75 C7.84 6.20 7.99 5.55 7.99 4.81 C7.99 4.07 7.84 3.42 7.55 2.86 C7.26 2.30 6.86 1.88 6.35 1.58 C5.84 1.28 5.27 1.13 4.63 1.13 C3.98 1.13 4.63 1.13 4.63 1.13 Z M16.63 0.12 C16.63 1.11 16.63 1.11 16.63 1.11 L13.97 1.11 L13.97 9.52 L12.76 9.52 L12.76 1.11 L10.11 1.11 L10.11 0.12 L16.63 0.12 L16.63 0.12 Z M19.04 5.73 C19.04 9.52 19.04 9.52 19.04 9.52 L17.83 9.52 L17.83 0.12 L20.81 0.12 C20.81 0.12 22.69 0.37 23.23 0.87 C23.76 1.38 24.03 2.06 24.03 2.93 C24.03 3.81 23.75 4.50 23.20 4.99 C22.65 5.49 20.81 5.73 20.81 5.73 L19.04 5.73 L19.04 5.73 Z M20.71 4.72 C20.71 4.72 21.98 4.56 22.30 4.25 C22.62 3.94 22.79 3.50 22.79 2.93 C22.79 2.35 22.62 1.90 22.30 1.59 C21.98 1.29 20.71 1.13 20.71 1.13 L19.04 1.13 L19.04 4.72 L20.71 4.72 C20.71 4.72 20.71 4.72 20.71 4.72 Z"
                  }
                />
              </Svg>
              <View style={styles.group29}>
                <Svg
                  viewBox={"-0.5 -0.5 47.0127877237852 22.460358056266"}
                  style={styles.stroke12}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={
                      "M42.97 20.46 C2.05 20.46 2.05 20.46 2.05 20.46 C0.91 20.46 0.00 19.54 0.00 18.41 L0.00 2.05 C0.00 0.92 0.91 0.00 2.05 0.00 L42.97 0.00 C44.10 0.00 45.01 0.92 45.01 2.05 L45.01 18.41 C45.01 19.54 44.10 20.46 42.97 20.46 L42.97 20.46 Z"
                    }
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 4.046035805626601 10.1841432225064"}
                  style={styles.stroke32}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M1.02 0.00 C1.02 8.18 1.02 8.18 1.02 8.18 L1.02 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879798 6.092071611253198"}
                  style={styles.stroke52}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 0.00 C6.14 4.09 6.14 4.09 6.14 4.09 L0.00 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879798 6.092071611253198"}
                  style={styles.stroke71}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 0.00 C6.14 4.09 6.14 4.09 6.14 4.09 L0.00 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879798 6.092071611253198"}
                  style={styles.stroke91}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 4.09 C6.14 0.00 6.14 0.00 6.14 0.00 L0.00 4.09 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 4.046035805626602 10.1841432225064"}
                  style={styles.stroke11}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M1.02 0.00 C1.02 8.18 1.02 8.18 1.02 8.18 L1.02 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879798 6.092071611253198"}
                  style={styles.stroke13}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 0.00 C6.14 4.09 6.14 4.09 6.14 4.09 L0.00 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879798 6.092071611253198"}
                  style={styles.stroke15}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 0.00 C6.14 4.09 6.14 4.09 6.14 4.09 L0.00 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879798 6.092071611253198"}
                  style={styles.stroke17}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 4.09 C6.14 0.00 6.14 0.00 6.14 0.00 L0.00 4.09 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 4.046035805626598 10.1841432225064"}
                  style={styles.stroke19}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M1.02 0.00 C1.02 8.18 1.02 8.18 1.02 8.18 L1.02 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879802 6.092071611253198"}
                  style={styles.stroke21}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 0.00 C6.14 4.09 6.14 4.09 6.14 4.09 L0.00 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879802 6.092071611253198"}
                  style={styles.stroke23}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 0.00 C6.14 4.09 6.14 4.09 6.14 4.09 L0.00 0.00 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879802 6.092071611253198"}
                  style={styles.stroke25}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 4.09 C6.14 0.00 6.14 0.00 6.14 0.00 L0.00 4.09 Z"}
                  />
                </Svg>
                <Svg
                  viewBox={"-0.5 -0.5 8.138107416879805 4.0460358056266"}
                  style={styles.stroke27}
                >
                  <Path
                    strokeWidth={1}
                    fill={"transparent"}
                    stroke={"rgba(9,32,88,1)"}
                    d={"M0.00 1.02 C6.14 1.02 6.14 1.02 6.14 1.02 L0.00 1.02 Z"}
                  />
                </Svg>
              </View>
            </View>
          </View>
          <View style={styles.style8}>
            <Svg viewBox={"-0 -0 80 80"} style={styles.mask}>
              <Path
                strokeWidth={0}
                fill={"rgba(255,255,255,1)"}
                d={
                  "M40.00 80.00 C62.09 80.00 80.00 62.09 80.00 40.00 C80.00 17.91 62.09 0.00 40.00 0.00 C17.91 0.00 0.00 17.91 0.00 40.00 C0.00 62.09 17.91 80.00 40.00 80.00 C62.09 80.00 40.00 80.00 40.00 80.00 Z"
                }
              />
            </Svg>
            <Image
              viewBox={"0 0 80 80"}
              svgDims={{
                x: "0",
                y: "0",
                height: "80",
                width: "80"
              }}
              style={styles.bitmap}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)"
  },
  multiStoreySecurit: {
    top: "80.56%",
    left: "0.00%",
    width: "100.00%",
    height: 72,
    backgroundColor: "transparent",
    color: "rgba(79,108,141,1)",
    position: "absolute",
    fontSize: 17,
    textAlign: "center"
  },
  absoluteSafety: {
    top: "59.65%",
    left: "19.00%",
    width: "62.01%",
    height: 62,
    backgroundColor: "transparent",
    color: "rgba(9,32,88,1)",
    position: "absolute",
    fontSize: 22,
    textAlign: "center"
  },
  icon: {
    top: "0.13%",
    left: "10.39%",
    width: "82.80%",
    height: "50.94%",
    position: "absolute"
  },
  style5: {
    top: "15.79%",
    left: "13.20%",
    width: "69.26%",
    height: "84.21%",
    position: "absolute"
  },
  oval: {
    top: "0.00%",
    left: "0.00%",
    width: "100.00%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  group7: {
    top: "25.94%",
    left: "31.08%",
    width: "37.50%",
    height: "48.13%",
    position: "absolute"
  },
  stroke1: {
    top: "0.19%",
    left: "0.24%",
    width: "102.08%",
    height: "9.27%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke3: {
    top: "6.86%",
    left: "0.24%",
    width: "102.08%",
    height: "94.25%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke5: {
    top: "29.46%",
    left: "26.84%",
    width: "56.04%",
    height: "35.12%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  style6: {
    top: "68.42%",
    left: "4.55%",
    width: "25.97%",
    height: "31.58%",
    position: "absolute"
  },
  oval1: {
    top: "0.00%",
    left: "0.00%",
    width: "100.00%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  group11: {
    top: "25.00%",
    left: "29.17%",
    width: "41.67%",
    height: "50.00%",
    position: "absolute"
  },
  stroke11: {
    top: "29.60%",
    left: "35.89%",
    width: "8.60%",
    height: "45.25%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke31: {
    top: "0.42%",
    left: "15.50%",
    width: "73.00%",
    height: "52.50%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke51: {
    top: "12.92%",
    left: "30.50%",
    width: "43.00%",
    height: "40.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke7: {
    top: "58.75%",
    left: "40.50%",
    width: "23.00%",
    height: "19.17%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke9: {
    top: "70.15%",
    left: "40.50%",
    width: "23.00%",
    height: "20.26%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  style7: {
    top: "36.84%",
    left: "65.15%",
    width: "34.63%",
    height: "42.11%",
    position: "absolute"
  },
  oval2: {
    top: "0.00%",
    left: "0.00%",
    width: "100.00%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  group2: {
    top: "24.38%",
    left: "20.00%",
    width: "60.00%",
    height: "51.25%",
    position: "absolute"
  },
  path: {
    top: "1.46%",
    left: "24.66%",
    width: "50.06%",
    height: "23.48%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  group29: {
    top: "44.45%",
    left: "0.67%",
    width: "98.04%",
    height: "54.89%",
    position: "absolute"
  },
  stroke12: {
    top: "2.32%",
    left: "1.11%",
    width: "99.90%",
    height: "99.80%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke32: {
    top: "29.60%",
    left: "14.15%",
    width: "8.60%",
    height: "45.25%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke52: {
    top: "38.69%",
    left: "9.81%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke71: {
    top: "38.69%",
    left: "9.81%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke91: {
    top: "38.69%",
    left: "9.81%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke13: {
    top: "38.69%",
    left: "31.55%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke15: {
    top: "38.69%",
    left: "31.55%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke17: {
    top: "38.69%",
    left: "31.55%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke19: {
    top: "29.60%",
    left: "57.63%",
    width: "8.60%",
    height: "45.25%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke21: {
    top: "38.69%",
    left: "53.29%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke23: {
    top: "38.69%",
    left: "53.29%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke25: {
    top: "38.69%",
    left: "53.29%",
    width: "17.29%",
    height: "27.07%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke27: {
    top: "61.41%",
    left: "75.02%",
    width: "17.29%",
    height: "17.98%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  style8: {
    top: "0.00%",
    left: "0.00%",
    width: "35.06%",
    height: "42.11%",
    position: "absolute"
  },
  mask: {
    top: "0.00%",
    left: "0.62%",
    width: "98.77%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  bitmap: {
    top: "0.00%",
    left: "0.62%",
    width: "98.77%",
    height: "100.00%",
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  }
});
