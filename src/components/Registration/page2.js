import React, { useState, useEffect } from "react";
import Colors from 'helpers/Colors';
import { StyleSheet, View, Text, Modal } from "react-native";
import { H2, Button, Icon, Spinner } from 'native-base';
import strings from 'localization';
import CameraModal from './cameraModal';
import { idClient } from '../../controllers/HttpClient';
import selfie from 'assets/images/selfie.js';
import id_front from 'assets/images/id_front.js';
import id_back from 'assets/images/id_back.js';
import milan from 'assets/images/milan.js';
import axios from 'axios';

const PendingView = () => (
  <View
    style={{
      flex: 1,
      backgroundColor: Colors.white,
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <Text>Waiting for camera...</Text>
  </View>
);


export default function Page2 (props) {
  const [idModal, setIdModal] = useState({visible: false, complete: false, image: null});
  const [backIdModal, setBackIdModal] = useState({visible: false, complete: false, image: null});
  const [selfieModal, setSelfieModal] = useState({visible: false, complete: false, image: null});
  const [verified, setVerified] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [verifyText, setVerifyText] = useState('');
  
  const handleImage = async () => {
    setLoading(true);
    idClient.post(
      '/',
        {
          "payload":{
            "imgType": "IdDocument",
            "imgData": idModal.image,
            "selfieData": selfieModal.image,
            "idBackData": "",
            "userId": "cf5dad4027e5ac6bcd908ec0d8d008a4ecece403c604f574e8205a516b6824b3"
          }
        }
    ).then(res => {
      console.log(res.data.payload)
      setVerified(res.data.payload.verified)
      if(res.data.payload.verified){
        setVerifyText('Probability: ' + res.data.payload.probability / 10 + '%');
      }
      else {
        setVerifyText('Verification failed. Please try again.');
        setSelfieModal({visible: false, complete: false, image: null});
        setIdModal({visible: false, complete: false, image: null});
        setBackIdModal({visible: false, complete: false, image: null});
      }
      setLoading(false)
    }).catch(err => {
      setVerified(false)
      setLoading(false)
      setVerifyText('Verification failed. Please try again.');
    })
    // Data is the object exposes by axios for the response json
  }

  useEffect(() => {
    if(idModal.complete && selfieModal.complete){
      props.setDisableNext(false);
    }
  })
  return (
    <View style={styles.root}>
      <Modal
        animationType="slide"
        transparent={false}
        visible={idModal.visible}>
        <CameraModal type="id" modal={setIdModal} handleImage={handleImage}/>
      </Modal>
      <Modal
        animationType="slide"
        transparent={false}
        visible={backIdModal.visible}>
        <CameraModal type="id" modal={setBackIdModal} handleImage={handleImage}/>
      </Modal>
      <Modal
        animationType="slide"
        transparent={false}
        visible={selfieModal.visible}>
        <CameraModal type="selfie" modal={setSelfieModal} handleImage={handleImage}/>
      </Modal>
      <H2 style={styles.title}>{strings.registration.page2.title}</H2>
      <Text style={styles.instructions}>{strings.registration.page2.instructions}</Text>
      <View style={styles.iconContainer}>
        <Button transparent onPress={() => setIdModal({...idModal,visible: true})} style={styles.tappable}>
          <View style={styles.iconRow}>
            <Icon style={[styles.icon, {color: idModal.complete ? Colors.success : Colors.gray}]} type='EvilIcons' name='check'/>
            <Text style={[styles.iconText, {color: idModal.complete ? Colors.success : Colors.gray}]}>{strings.registration.page2.id.toUpperCase()}</Text>
          </View>
        </Button>
        <Button transparent onPress={() => setBackIdModal({...backIdModal,visible: true})} style={styles.tappable}>
          <View style={styles.iconRow}>
            <Icon style={[styles.icon, {color: backIdModal.complete ? Colors.success : Colors.gray}]} type='EvilIcons' name='check'/>
            <Text style={[styles.iconText, {color: backIdModal.complete ? Colors.success : Colors.gray}]}>{strings.registration.page2.backId.toUpperCase()}</Text>
          </View>
        </Button>
        <Button transparent onPress={() => setSelfieModal({...selfieModal,visible: true})} style={styles.tappable}>
          <View style={styles.iconRow}>
            <Icon style={[styles.icon, {color: selfieModal.complete ? Colors.success : Colors.gray}]} type='EvilIcons' name='check'/>
            <Text style={[styles.iconText, {color: selfieModal.complete ? Colors.success : Colors.gray}]}>{strings.registration.page2.selfie.toUpperCase()}</Text>
          </View>
        </Button>
      </View>
      <View style={styles.verifyContainer}>
        { !verified && <Button full onPress={() => handleImage()}>
          {isLoading && <Spinner color={Colors.white} size="small"/>}
          <Text style={[styles.verifyText, {marginLeft: 10}]}>{ isLoading ? 'Verifying' : 'Verify'}</Text>
        </Button> }
        { verified && <Button full disabled style={{backgroundColor: Colors.success}} onPress={() => handleImage()}>
          <Icon type="EvilIcons" style={styles.checkIcon} name="check"/>
          <Text style={styles.verifyText}>Verified</Text>
        </Button> }
        <Text style={[styles.verifyResultText, {color: verified ? Colors.black : Colors.error}]}>{verifyText}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingHorizontal: 20,
  },
  title: {
    textAlign: 'center'
  },
  instructions: {
    color: Colors.black,
    marginTop: 20,
    fontSize: 16,
    textAlign: 'center',
  },
  iconContainer: {
    left: '20%',
    marginTop: 20,
  },
  iconRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    fontSize: 70,
    color: Colors.gray,
    marginLeft: 0
  },
  iconText: {
    fontSize: 17,
    color: Colors.gray
  },
  tappable: {
    height: 'auto',
    margin: 0,
    padding: 0,
  },
  verifyContainer: {
    marginTop: 10
  },
  verifyText: {
    color: Colors.white,
    fontSize: 20,
  },
  checkIcon: {
    color: Colors.white,
    fontSize: 40,
    marginLeft:0,
    marginRight: 0,
  },
  verifyResultText: {
    textAlign: 'center',
    marginTop: 10,
  }
});