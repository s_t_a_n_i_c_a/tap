import React, { Component } from "react";
import Colors from 'helpers/Colors';
import { StyleSheet, View, Text } from "react-native";
import { Icon, Button, H2 } from 'native-base';
import strings from 'localization';

export default function Page3(props) {
  return (
    <View style={styles.root}>
      <View style={{flex:1}}>
        <H2 style={styles.title}>{strings.registration.page3.title}</H2>
        <Text style={styles.instructions}>{strings.registration.page3.instructions}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  title: {
    textAlign: 'center'
  },
  instructions: {
    color: Colors.black,
    marginTop: 20,
    fontSize: 16,
    textAlign: 'center',
    paddingHorizontal: 20
  },
});
