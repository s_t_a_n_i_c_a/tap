import { StyleSheet } from 'react-native';
import Colors from 'helpers/Colors';
import { Dimensions } from 'react-native';

export const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightGray
  },
  logo: {
    width: '100%',
    height: 30,
    resizeMode: 'contain',
  },
  header: {
    backgroundColor: Colors.primary,
  },
  headerText: {
    color: Colors.white
  },
  nextBtn: {
    position: 'absolute',
    right: -40,
    bottom: -10,
  },
  prevBtn: {
    left: -40,
    bottom: -10,
  },
  finishButton: {
    right: -60 + (width/2)-38,
    bottom: 140,
    borderWidth: 2,
    borderColor: Colors.primary
  },
  loading: {
    right: -60 + (width/2)-48,
    bottom: 140,
    borderWidth: 2,
    borderColor: Colors.primary
  },
  btnText: {
    color: Colors.primary,
    letterSpacing: 2,
  }
});

export default styles;
