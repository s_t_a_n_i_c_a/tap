import React, { useCallback, useEffect, useState } from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  Alert,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import styles from './styles';
import logo from 'assets/images/icon.png';
import Colors from 'helpers/Colors';
import Page1 from './page1';
import Page2 from './page2';
import Page3 from './page3';
import Page4 from './page4';

import getUser from 'selectors/UserSelectors';
import strings from 'localization';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import { Header, Left, Body, Right } from 'native-base';
import { isLoadingSelector } from 'selectors/StatusSelectors';
import { login, actionTypes } from 'actions/UserActions';
import Biometrics from 'react-native-biometrics'

function Registration(props) {

  const user = useSelector(state => getUser(state));
  const [disableNext, setDisableNext] = useState(true);
  const dispatch = useDispatch();
  const isLoading = useSelector(state => isLoadingSelector([actionTypes.LOGIN], state));
  const loginUser = useCallback(() => dispatch(login('robert.stanica@intellitix.com', 'Matt123!')), [dispatch]);

  const checkBiometrics = () => {
        Biometrics.simplePrompt('Confirm Identity')
        .then(() => {
            loginUser();
        })
        .catch(() => {
            Alert.alert(
                'Authentication Required',
                'You need to authenticate yourself before we can topup your account.',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
        })
    }

  useEffect(() => {
    if (user !== null) {
      props.navigation.navigate('App');
    }
  });

  return (
    <View style={styles.container}>
      <StatusBar/>
      <Header style={styles.header}>
          <Left/>
          <Body>
            <Image source={logo} style={styles.logo}/>
          </Body>
          <Right />
        </Header>
        <ProgressSteps
          activeStepIconBorderColor={Colors.shadow}
          completedProgressBarColor={Colors.shadow}
          completedStepIconColor={Colors.shadow}
          activeStepIconColor={Colors.shadow}
          activeStepNumColor={Colors.white}
          disabledStepIconColor={Colors.gray}
          progressBarColor={Colors.gray}

        >
            <ProgressStep label="" nextBtnTextStyle={styles.btnText} nextBtnStyle={styles.nextBtn}>
              <Page1/>
            </ProgressStep>
            <ProgressStep label="" nextBtnDisabled={disableNext} nextBtnTextStyle={styles.btnText} previousBtnTextStyle={styles.btnText} previousBtnStyle={styles.prevBtn} nextBtnStyle={styles.nextBtn}>
              <Page2 setDisableNext={setDisableNext} />
            </ProgressStep>
            <ProgressStep previousBtnDisabled={true} previousBtnText="" finishBtnText= {isLoading ? strings.loading : 'Begin'} label="" onSubmit={() => checkBiometrics()} nextBtnTextStyle={styles.btnText} nextBtnStyle={isLoading ? styles.loading : styles.finishButton}>
             <Page3/>
            </ProgressStep>
        </ProgressSteps>
    </View>
  );
}

Registration.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Registration;