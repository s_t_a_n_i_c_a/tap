import React, { Component } from "react";
import Colors from 'helpers/Colors';
import { StyleSheet, View, Text, Image } from "react-native";
import { H2, Input, Item, Label } from 'native-base';
import strings from 'localization';

export default function Page1 (props) {
  return (
    <View accessibilityLabel="container" style={styles.root}>
      <H2 accessibilityLabel="register title" style={styles.title}>{strings.register}</H2>
      <Text accessibilityLabel="register instructions" style={styles.instructions}>{strings.registration.page1.instructions}</Text>
      <View accessibilityLabel="name input container" style={styles.formElement}>
        <Label accessibilityLabel="name input label" style={styles.formLabel}>{strings.registration.page1.name}</Label>
        <Item accessibilityLabel="name input form item" style={styles.formInput} regular>
          <Input accessibilityLabel="name input" />
        </Item>
      </View>
      <View style={styles.formElement}>
        <Label style={styles.formLabel}>{strings.registration.page1.surname}</Label>
        <Item style={styles.formInput} regular>
          <Input  />
        </Item>
      </View>
      <View style={styles.formElement}>
        <Label style={styles.formLabel}>{strings.registration.page1.email}</Label>
        <Item style={styles.formInput} regular>
          <Input  />
        </Item>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingHorizontal: 20
  },
  title: {
    textAlign: 'center'
  },
  instructions: {
    color: Colors.black,
    marginTop: 20,
    fontSize: 16,
    textAlign: 'center',
  },
  formElement: {
    marginTop: 30
  },
  formLabel: {
    color: Colors.black,
    fontWeight: 'bold'
  },
  formInput: {
    marginTop: 5,
    backgroundColor: Colors.white
  }
});