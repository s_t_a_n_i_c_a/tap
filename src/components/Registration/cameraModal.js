import React, { useState } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { Button, Icon } from 'native-base';
import { RNCamera } from 'react-native-camera';

const PendingView = () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'lightgreen',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <Text>Waiting</Text>
  </View>
);


export default function CameraModal (props) {
    const [showCamera, setShowCamera] = useState(true);
    const [img, setImg] = useState();
    takePicture = async (camera) => {
        const options = { quality: 0.5, base64: true };
        const data = await camera.takePictureAsync(options);
        //  eslint-disable-next-line
        console.log(data.uri);
        setShowCamera(false);
        setImg(data.base64);
    };
    confirm = function() {
      props.modal({visible: false, complete: true, image: img});
    }
    retake = function() {
        setShowCamera(true);
    }
    return (
    <View style={styles.root}>
        <View style={[styles.imageView, {display: showCamera ? 'none' : 'flex'}]}>
            <Image style={[styles.image,{display: img ? 'flex' : 'none'}]} source={{uri: 'data:image/png;base64,' + img}}></Image>
            <View style={styles.buttonContainer}>
                <Button style={styles.capture} transparent onPress={() => retake()}>
                    <Icon style={styles.icon} active name='redo' type='EvilIcons'></Icon>
                </Button>
                <Button style={styles.capture} transparent onPress={() => confirm()}>
                    <Icon style={styles.icon} active name='check' type='EvilIcons'></Icon>
                </Button>
            </View>
        </View>
        <RNCamera
            style={[styles.preview, {display: showCamera ? 'flex' : 'none'}]}
            type={props.type === 'id' ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front}
            androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
                title: 'Permission to use audio recording',
                message: 'We need your permission to use your audio',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
            }}
            >
            {({ camera, status }) => {
                if (status !== 'READY') return <PendingView />;
                return (
                <View style={styles.buttonContainer}>
                    <Button transparent onPress={() => takePicture(camera)} style={styles.capture}>
                        <Icon style={styles.icon} active name="camera" type='EvilIcons'></Icon>
                    </Button>
                </View>
                );
            }}
        </RNCamera>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  preview: {
    flex: 2,
    justifyContent: 'flex-end',
  },
  buttonContainer: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20
  },
  image: {
    flex: 1,
  },
  imageView: {
      flex: 1
  },
  capture: {
    flex: 0,
    backgroundColor: 'transparent',
    height: 'auto',
    justifyContent: 'center'
  },
    icon: {
        fontSize: 80,
        color: 'rgba(255,255,255,0.8)'
  }
});