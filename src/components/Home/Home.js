import React, { useCallback, useEffect, useState } from 'react';
import { View, ScrollView, Text, Image, StatusBar, TouchableOpacity, ImageBackground, StyleSheet} from 'react-native';
import { Container, Header, Item, Input, Icon, Button } from 'native-base';
import { useSelector, useDispatch } from 'react-redux';
import Colors from 'helpers/Colors';
import styles from './styles';

// import Ticket from './Ticket';
import strings from 'localization';
import getUser from 'selectors/UserSelectors';
import getEvents from 'selectors/EventsSelector';
import { search } from 'actions/EventsActions';


const data = [
  {
    id: 1,
    title: 'Tomorrowland',
    image: 'https://www.tomorrowland.com/src/Frontend/Themes/tomorrowland/Core/Layout/images/timeline/2017-1.jpg',
    month: 'October',
    day: 14,
    location: 'Belgium',
    place: 'De Schorre',
    year: 2019,
    about: 'Tomorrowland is an electronic dance music festival held in Boom, Belgium. Tomorrowland was first held in 2005 and has since become one of the world\'s...'
  },
  {
    id: 2,
    title: 'Veld',
    image: 'https://media.blogto.com/articles/veld-new.JPG?w=2048&cmd=resize_then_crop&height=1365&quality=70',
    month: 'November',
    day: 7,
    location: 'Toronto',
    place: 'Downsview Park',
    year: 2019,
    about: 'VELD Music Festival is an annual electronic dance music and hip-hop music festival held at Downsview Park in Toronto, Ontario. Since its inception in 2012...',
  },
  {
    id: 3,
    title: 'Coachella',
    image: 'https://consequenceofsound.net/wp-content/uploads/2019/04/Coachella-2019-YouTube-STreaming-Schedule-e1555079789554.png?w=800',
    month: 'April',
    day: 19,
    location: 'California',
    place: 'Empire Polo Club',
    year: 2020,
    about: 'The Coachella Valley Music and Arts Festival is an annual music and arts festival held at the Empire Polo Club in Indio, California, in the Coachella Valley...',
  },
  {
    id: 4,
    title: 'Meadows in the Mountains',
    image: 'https://d49r1np2lhhxv.cloudfront.net/image/1400x1050/center/middle/filters:quality(70)/www/admin/uploads/images/Top50MusicFestivalsintheWorld_MINM.jpg',
    month: 'June',
    day: 4,
    location: 'Bulgaria',
    place: 'Polkovnik',
    year: 2019,
    about: 'The monastery bell chimes to the 10th chapter of Meadows In The Mountains Festival and what an epic 10 years it has been. Another time we will tell the story...',
  }
];

const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function Home(props) {
  const user = useSelector(state => getUser(state));
  let events = useSelector(state => getEvents(state));
  const dispatch = useDispatch();
  const _search = useCallback((device) => dispatch(search(device)), []);
  const [query, setQuery] = useState('');
  const queryChanged = useCallback(value => {setQuery(value); dispatch(search(user.device, value)), []});
  const parseTitle = (title) => {
    if(title.length > 18){
      return title.substring(0,18) + '...';
    }
    return title;
  }

  useEffect(() => {
    console.log(events)
    if (user === null || user.error) {
      props.navigation.navigate('Login');
    }
    _search();
  }, []);

  return (
    <Container>
        <Header searchBar rounded style={styles.searchBar}>
          <Item style={{backgroundColor: Colors.white}}>
            <Icon name="ios-search" />
            <Input
              value={query}
              onChangeText={queryChanged}
              placeholder="Search" />
          </Item>
        </Header>
      <StatusBar barStyle="light-content" />
      {/* <Tabs tabBarUnderlineStyle={{backgroundColor: Colors.primary}} locked>
        <Tab heading={strings.wallet.tab1}> */}
          <ScrollView style={styles.ticketContainer}>
            {events.events.map((event,i) => {
              return (
                <TouchableOpacity key={i} style={eventStyles.root}>
                  <ImageBackground source={{uri: event.image || 'https://fakeimg.pl/350x220/?text=' + event.name}} style={eventStyles.imageBackground}>
                      <View style={eventStyles.overlay}></View>
                      <View style={eventStyles.dateContainer}>
                          <View style={[eventStyles.dateBox, eventStyles.dateBoxLeft, {display: event.startdate ? 'flex' : 'none'}]}>
                              <Text style={eventStyles.day}>{('0' + new Date((event.startdate + event.offsetUTC) * 1000).getDate()).slice(-2)}</Text>
                              <Text style={eventStyles.month}>{months[new Date(event.startdate).getMonth()].substring(0,3).toUpperCase()}</Text>
                          </View>
                          <View style={{display: event.startdate ? 'flex' : 'none', backgroundColor: 'white', paddingTop: 12}}><Text style={{fontWeight:'bold', color: Colors.black}}>-</Text></View>
                          <View style={[eventStyles.dateBox, eventStyles.dateBoxRight, {display: event.startdate ? 'flex' : 'none'}]}>
                              <Text style={eventStyles.day}>{('0' + new Date((event.enddate + event.offsetUTC) * 1000).getDate()).slice(-2)}</Text>
                              <Text style={eventStyles.month}>{months[new Date(event.enddate).getMonth()].substring(0,3).toUpperCase()}</Text>
                          </View>
                      </View>
                      <View style={eventStyles.lowerContainer}>
                          <Text style={eventStyles.location}>{event.timezone.toUpperCase().split('/')[1]}</Text>
                          <Text style={eventStyles.title}>{parseTitle(event.name)}</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center'}}>
                              <Icon style={eventStyles.icon} type='FontAwesome5' name='sitemap'/>
                              <Text style={eventStyles.place}>{event.organization}</Text>
                          </View>
                      </View>
                  </ImageBackground>
                </TouchableOpacity>
              )
            })}
          </ScrollView>
        {/* </Tab>
        <Tab heading={strings.wallet.tab2}>
          <Text></Text>
        </Tab>
      </Tabs> */}
    </Container>
  );
}

const eventStyles = StyleSheet.create({
  root: {
      height: 220,
      shadowColor: 'rgba(58,55,55,1)',
      shadowOffset: { width: 0, height: 7 },
      shadowOpacity: 0.5,
      shadowRadius: 5,
      padding: 10,
  },
  imageBackground: {
      flex: 1,
      height: 220,
      overflow: 'hidden',
      borderRadius: 20,
      justifyContent: 'space-between',
  },
  overlay: {
      position: 'absolute',
      backgroundColor: 'rgba(0,0,0,0.6)',
      left: 0,
      top: 0,
      width: '100%',
      height: '100%',
      borderRadius: 20,
  },
  dateBox: {
      backgroundColor: Colors.white,
      justifyContent: 'space-between',
      paddingVertical: 8,
      alignItems: 'center',
  },
  dateBoxLeft: {
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    paddingLeft: 8,
    paddingRight: 2,
  },
  dateBoxRight: {
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    paddingRight: 8,
    paddingLeft: 2,
  },
  dateContainer: {
      justifyContent: 'flex-end',
      flexDirection:'row',
      padding: 10
  },
  lowerContainer: {
      justifyContent: 'flex-end',
      padding: 15,
  },
  day: {
      fontSize: 18,
      color: Colors.black
  },
  month: {
      fontSize: 10,
      color: Colors.black,
      fontWeight: 'bold',
      color: 'rgba(0,0,0,0.7)',
  },
  image: {
      position: 'absolute',
      left: 10,
      top: 0,
  },
  location: {
      fontSize: 10,
      color: Colors.white,
  },
  title: {
      fontSize: 30,
      color: Colors.white,
      letterSpacing: 2
  },
  place: {
      color: Colors.white
  },
  icon: {
      fontSize: 12,
      color: Colors.white,
      paddingRight: 5,
      marginLeft: 0,
      marginRight: 0
  }
});

export default Home;
