import { StyleSheet } from 'react-native';
import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.lightGray,
  },
  searchBar: {
    backgroundColor: Colors.primary
  },
  header: {
    color: Colors.white
  },
  logo: {
    width: '100%',
    height: 30,
    resizeMode: 'contain',
  },
  ticketContainer: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: Colors.white,
    alignSelf: 'stretch'
  }
});

export default styles;
