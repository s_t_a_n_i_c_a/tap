export default {
  login: 'Login',
  device: 'Device Name',
  register: 'Register',
  create: 'Create Account',
  loading: 'Loading',
  password: 'Password',
  email: 'Email',
  walletTab: 'Wallet',
  homeMessage: 'Welcome',
  profile: 'Profile',
  profileMessage: 'And this one has a nav, but with a different color than the login.',
  logout: 'Logout',
  onboarding: {
    page1: {
      header: 'Welcome To Your Festival App',
      instructions: 'What to know what you can do?',
      instructionsSubtext: 'Take the tour'
    },
    page2: {
      header: 'Account Protection',
      instructions: 'We take safety and security very seriously with our identity verification and biometric authentication. Safely store your ticket and use it for a fast check-in at the gate'
    },
    page3: {
      header: 'Personalized offers',
      instructions: 'Get personalized (or the best) offers and discounts from the festival and concession stands'
    },
    page4: {
      instructions: 'Ready to take your festival experience to the next level?',
      instructionsSubtext: 'By signing up you are agreeing to the club\'s terms of service and privacy policy'
    }
  },
  registration: {
    page1: {
      instructions: 'Fill in your name and email address to create you Intellitix account and boost your experience',
      name: 'Name',
      surname: 'Surname',
      email: 'Email'
    },
    page2: {
      title: 'Verify Your Id',
      instructions: 'We want to tie your unique identity to your fan ID as a trust anchor for secure access throughout your account lifecycle. Your mobile phone will be an intelligent connected identity device',
      id: 'Front Id',
      selfie: 'Selfie',
      backId: 'Back Id'
    },
    page3: {
      title: 'Biometric Confirmation Required',
      instructions: 'We need to confirm your biometrics. Please tap the button below to start your festival experience.',
      button: 'Begin'
    }
  },
  wallet: {
    title: 'My Wallet',
    tab1: 'My Tickets',
    tab2: 'Transfer/Add'
  }
};
